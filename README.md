# MiniProjet POO Jamin Avenel

Ce mini-projet de Programmation Orientée Objet (POO) a été réalisé par Théophile Avenel et Mathis Jamin. Ce repository GitLab contient les différentes versions du code, reflétant l'évolution et l'amélioration progressive du projet.

## Structure du Repository

### 1. Première Version
- **Dossier : `premiereversion`**
  - **Description :** Cette version contient le code initial du projet avec `Produit.cpp` et le code principal dans le même fichier.

### 2. Amélioration de l'Architecture
- **Dossier : `miniprojetPOOStepByStep`**
  - **Description :** Ce dossier représente la première refonte de l'architecture du code, apportant une structure plus claire et modulaire.

### 3. Gestion des Boucles Infinies et Renforcement du Code
- **Dossier : `miniprojetPOOInfinite`**
  - **Description :** Améliorations pour corriger les boucles infinies et renforcer la robustesse du code.

### 4. Fin de l'Étape 4 - Achats
- **Dossier : `miniprojetPOOAvantVectorComplet`**
  - **Description :** Cette version marque la fin de l'étape 4 sur les Achats avant d'aborder les interactions.

### 5. Étape des Interactions
- **Dossier : `miniprojetPOOAvantVectorComplet`**
  - **Description :** Le projet a été complexifié pour inclure les interactions avant de passer à la base de données (partie optionnelle).

### 6. Test de SQLite
- **Dossier : `testsqlite`**
  - **Description :** Code utilisé pour tester l'intégration de SQLite dans le projet.

### 7. Évolution du Code
- **Dossier : `projectvar`**
  - **Description :** Ce dossier contient une version évoluée du projet avec des améliorations continues.

### 8. Code Final
- **Dossier : `miniprojetPOO`**
  - **Description :** Version finale du projet, intégrant toutes les fonctionnalités et optimisations apportées au cours des différentes étapes.

## Instructions pour Utiliser le Projet

1. **Cloner le Repository :**
   ```bash
   git clone <URL_du_repository>
