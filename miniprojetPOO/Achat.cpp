#include "Achat.h"
#include <iostream>

// Initialisation de la variable statique
int Achat::dernierNumeroAchat = 0;

// Constructeur de la classe Achat
Achat::Achat(int date) : dateAchat(date), personne(nullptr) {
    numeroAchat = ++dernierNumeroAchat; // Incrémente et attribue le numéro d'achat
}

Achat::~Achat() {
    std::cout << "Le destructeur va supprimer l'objet de type Achat." << std::endl;
}

// Méthode pour ajouter un produit dans le vecteur des produits achetés
void Achat::ajouterProduitAchat(Produit* produit) {
    produits.push_back(produit);
}

// Méthode pour ajouter une personne à l'achat
void Achat::ajouterPersonneAchat(Personne* personne) {
    this->personne = personne;
}

// Méthode pour afficher l'achat réalisé
void Achat::afficherDetails() const {
    std::cout << "Date de l'achat : " << dateAchat << std::endl;
    std::cout << "Produits achetés : " << std::endl;
    for (const auto& produit : produits) {
        produit->afficher();
    }
    if (personne) {
        std::cout << "Personne concernée : " << personne->getNom() << std::endl;
    } else {
        std::cout << "Personne concernée : Non définie" << std::endl;
    }
}

int Achat::getNumeroAchat() const {
    return numeroAchat;
}

// Setters
void Achat::setDateAchat(int date) {
    dateAchat = date;
}

void Achat::setNumeroAchat(int numero) {
    numeroAchat = numero;
}

// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Achat& achat) {
    os << "Numero d'achat: " << achat.numeroAchat << "\nDate d'achat: " << achat.dateAchat << "\nProduits:\n";
    for (const auto& produit : achat.produits) {
        os << "\t" << *produit << "\n";
    }
    if (achat.personne) {
        os << "Personne: " << *achat.personne << "\n";
    } else {
        os << "Personne: Non définie\n";
    }
    return os;
}

// Surcharge de l'opérateur ==
bool Achat::operator==(const Achat& autre) const {
    if (dateAchat != autre.dateAchat || numeroAchat != autre.numeroAchat || produits.size() != autre.produits.size()) {
        return false;
    }
    for (size_t i = 0; i < produits.size(); ++i) {
        if (*produits[i] != *autre.produits[i]) {
            return false;
        }
    }
    return personne == autre.personne || (personne && autre.personne && *personne == *autre.personne);
}

// Surcharge de l'opérateur !=
bool Achat::operator!=(const Achat& autre) const {
    return !(*this == autre);
}

// Création des tables dans la base de données
void Achat::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS ACHAT ("
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    "DATE INTEGER, "
                    "PERSONNE INTEGER, "
                    "FOREIGN KEY(PERSONNE) REFERENCES PERSONNE(NUMEROSECURITESOCIALE));");

    db.executeQuery("CREATE TABLE IF NOT EXISTS ACHAT_PRODUITS ("
                    "ACHAT_ID INTEGER, "
                    "PRODUIT_ID INTEGER, "
                    "FOREIGN KEY(ACHAT_ID) REFERENCES ACHAT(ID), "
                    "FOREIGN KEY(PRODUIT_ID) REFERENCES PRODUIT(NUMEROSERIE));");
}

// Sauvegarde des achats dans la base de données
void Achat::saveToDatabase(Database& db) const {
    if (personne == nullptr) {
        throw std::runtime_error("Personne non définie pour cet achat.");
    }

    std::string query = "INSERT INTO ACHAT (DATE, PERSONNE) VALUES ("
                        + std::to_string(dateAchat) + ", "
                        + std::to_string(personne->getNumerosecuritesociale()) + ");";
    db.executeQuery(query);

    for (const auto& produit : produits) {
        std::string produitQuery = "INSERT INTO ACHAT_PRODUITS (ACHAT_ID, PRODUIT_ID) VALUES ("
                                   "(SELECT last_insert_rowid()), "
                                   + std::to_string(produit->getNumeroSerie()) + ");";
        db.executeQuery(produitQuery);
    }
}
