#ifndef ACHAT_H
#define ACHAT_H

#include <iostream>
#include <string>
#include <vector>
#include "Personne.h"
#include "Produit.h"
#include "constants.h"
#include "useful.h"
#include "Database.h"

class Achat {
private:
    static int dernierNumeroAchat; // Variable statique pour suivre le dernier numéro d'achat
    int dateAchat; // Année d'achat
    int numeroAchat;
    std::vector<Produit*> produits; // Utilisation d'un vecteur pour la flexibilité
    Personne* personne; // Référence à une seule personne associée à l'achat

public:
    Achat(int date);
    ~Achat();

    void ajouterProduitAchat(Produit* produit);
    void ajouterPersonneAchat(Personne* personne);
    void afficherDetails() const;
    int getNumeroAchat() const;

    // Setters
    void setDateAchat(int date);
    void setNumeroAchat(int numero);

    friend std::ostream& operator<<(std::ostream& os, const Achat& achat);
    bool operator==(const Achat& autre) const;
    bool operator!=(const Achat& autre) const;

    // DATABASE
    static void createTable(Database& db);
    void saveToDatabase(Database& db) const;
};

#endif // ACHAT_H
