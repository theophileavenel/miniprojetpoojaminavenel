#include "Database.h"
#include <iostream>

// Callback function for SQL execution
int Database::callback(void* data, int argc, char** argv, char** azColName) {
    int i;
    fprintf(stderr, "%s: ", (const char*)data);
    for (i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

// Constructeur: ouvrir la Base de donénes QSLITE
Database::Database(const std::string& dbName) {
    rc = sqlite3_open(dbName.c_str(), &db);
    if (rc) {
        std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        exit(0);
    } else {
        std::cout << "Opened database successfully" << std::endl;
    }
}

// Destructeur: fermer la base de donénes SQLITE
Database::~Database() {
    sqlite3_close(db);
}

// Méthode pour exécuter une requête SQL sans retourner de résultats
void Database::executeQuery(const std::string& query) {
    rc = sqlite3_exec(db, query.c_str(), nullptr, nullptr, &errMsg);
    if (rc != SQLITE_OK) {
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
    } else {
        std::cout << "Query executed successfully" << std::endl;
    }
}

// Méthode pour executer une commande SQL avec l'appel à la focntion callback
void Database::executeSQL(const std::string& sql) {
    rc = sqlite3_exec(db, sql.c_str(), callback, (void*)data, &errMsg);
    if (rc != SQLITE_OK) {
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
    } else {
        std::cout << "Operation done successfully" << std::endl;
    }
}

// Le getter pour récupérer db
sqlite3* Database::getDB() const {
    return db;
}
