#ifndef DATABASE_H
#define DATABASE_H

#include <sqlite3.h>
#include <string>
#include <vector>
#include <stdexcept>

class Database {
public:
    Database(const std::string& dbName);
    ~Database();
    
    void executeQuery(const std::string& query);
    void executeSQL(const std::string& sql);

    sqlite3* getDB() const; // Ajoutez cette ligne

    static int callback(void* data, int argc, char** argv, char** azColName);

private:
    sqlite3* db;
    int rc;
    char* errMsg = 0;
    const char* data = "Callback function called";
};

#endif // DATABASE_H
