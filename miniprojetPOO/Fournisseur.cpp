#include "Fournisseur.h"

Fournisseur::Fournisseur(const std::string& n, const std::string& coord, int codePays, int numeroTel)
    : nom(n), coordonnees(coord), codePays(codePays), numeroTel(numeroTel) {}

Fournisseur::~Fournisseur() {
    std::cout << "Le destructeur va supprimer l'objet de type Fournisseur." << std::endl;
}

std::string Fournisseur::getNom() const {
    return nom;
}

std::string Fournisseur::getCoordonnees() const {
    return coordonnees;
}

int Fournisseur::getCodePays() const {
    return codePays;
}

int Fournisseur::getNumeroTel() const {
    return numeroTel;
}

std::vector<Produit*> Fournisseur::getProduitsFournisseur() const {
    return produits;
}

void Fournisseur::ajouterProduit(Produit* produit) {
    produits.push_back(produit);
}

void Fournisseur::ajouterProduitDepuisSaisie(std::vector<Produit*>& tousLesProduits) {
    int numeroSerie;
    std::cout << "Quel produit voulez-vous rentrer (numéro de série) ? ";
    std::cin >> numeroSerie;

    Produit* produit = trouverProduitParNumeroSerie(tousLesProduits, numeroSerie);
    if (produit != nullptr) {
        ajouterProduit(produit);
        std::cout << "Produit ajouté au fournisseur : " << produit->getNumeroSerie() << std::endl;
    } else {
        std::cout << "Produit avec le numéro de série " << numeroSerie << " non trouvé." << std::endl;
    }
}

Produit* Fournisseur::trouverProduitParNumeroSerie(const std::vector<Produit*>& tousLesProduits, int numeroSerie) {
    for (Produit* produit : tousLesProduits) {
        if (produit->getNumeroSerie() == numeroSerie) {
            return produit;
        }
    }
    return nullptr;
}

void Fournisseur::afficher() const {
    std::cout << "Nom du fournisseur : " << nom << std::endl;
    std::cout << "Coordonnées : " << coordonnees << std::endl;
    std::cout << "Téléphone : +" << codePays << " " << numeroTel << std::endl;
    std::cout << "Produits fournis :" << std::endl;
    for (const auto& produit : produits) {
        produit->afficher(); // Utiliser la méthode afficher() de la classe Produit
    }
}



Fournisseur* Fournisseur::creerFournisseur() {
    std::string nom, adresse;
    int codePays;
    long long numeroTel;

    // Saisir le nom du fournisseur et vérifier l'absence d'espaces
    do {
        std::cout << "Nom du fournisseur (sans espaces): ";
        std::getline(std::cin, nom);
        if (contientEspaces(nom) || nom.empty()) {
            std::cout << "Erreur: Le nom ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
        }
    } while (contientEspaces(nom) || nom.empty());

    // Saisir l'adresse du fournisseur et vérifier l'absence d'espaces
    do {
        std::cout << "Adresse (sans espaces): ";
        std::getline(std::cin, adresse);
        if (contientEspaces(adresse) || adresse.empty()) {
            std::cout << "Erreur: L'adresse ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
        }
    } while (contientEspaces(adresse) || adresse.empty());

    // Saisir le code pays et vérifier qu'il est valide
    std::cout << "Code pays (1-999): ";
    while (!(std::cin >> codePays) || codePays < 1 || codePays > 999) {
        std::cout << "Entrée invalide. Veuillez entrer un code pays valide (1-999): ";
        std::cin.clear();  // Effacer l'état d'erreur
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Ignorer les caractères restants
    }

    // Saisir le numéro de téléphone et vérifier qu'il est valide
    std::cout << "Numéro de téléphone (10 chiffres): ";
    while (!(std::cin >> numeroTel) || numeroTel < 1000000000LL || numeroTel > 9999999999LL) {
        std::cout << "Entrée invalide. Veuillez entrer un numéro de téléphone valide (10 chiffres): ";
        std::cin.clear();  // Effacer l'état d'erreur
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Ignorer les caractères restants
    }

    // Ignorer les caractères restants dans le buffer après les saisies numériques
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::cout << "Création de fournisseur avec succès.\n";
    return new Fournisseur(nom, adresse, codePays, numeroTel);
}


// Setters
void Fournisseur::setNom(const std::string& n) {
    nom = n;
}

void Fournisseur::setCoordonnees(const std::string& coord) {
    coordonnees = coord;
}

void Fournisseur::setCodePays(int code) {
    codePays = code;
}

void Fournisseur::setNumeroTel(int tel) {
    numeroTel = tel;
}

// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Fournisseur& fournisseur) {
    os << "Nom: " << fournisseur.nom << "\n";
    os << "Coordonnées: " << fournisseur.coordonnees << "\n";
    os << "Code Pays: " << fournisseur.codePays << "\n";
    os << "Numéro de Téléphone: " << fournisseur.numeroTel << "\n";
    os << "Produits: ";
    for (const auto& produit : fournisseur.produits) {
        os << *produit << " ";
    }
    os << "\n";
    return os;
}

bool Fournisseur::operator==(const Fournisseur& other) const {
    return (nom == other.nom &&
            coordonnees == other.coordonnees &&
            codePays == other.codePays &&
            numeroTel == other.numeroTel &&
            produits == other.produits);
}

bool Fournisseur::operator!=(const Fournisseur& autre) const {
    return !(*this == autre);
}

// Méthode pour créer la table Fournisseur dans la base de données
void Fournisseur::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS FOURNISSEUR ("
                    "NOM TEXT PRIMARY KEY, "
                    "COORDONNEES TEXT, "
                    "CODEPAYS INTEGER, "
                    "NUMEROTEL INTEGER);");

    db.executeQuery("CREATE TABLE IF NOT EXISTS FOURNISSEUR_PRODUITS ("
                    "FOURNISSEUR_NOM TEXT, "
                    "PRODUIT_NUMEROSERIE INTEGER, "
                    "FOREIGN KEY(FOURNISSEUR_NOM) REFERENCES FOURNISSEUR(NOM), "
                    "FOREIGN KEY(PRODUIT_NUMEROSERIE) REFERENCES PRODUIT(NUMEROSERIE));");
}

// Méthode pour sauvegarder un fournisseur dans la base de données
void Fournisseur::saveToDatabase(Database& db) {
    std::string query = "INSERT INTO FOURNISSEUR (NOM, COORDONNEES, CODEPAYS, NUMEROTEL) VALUES ('"
                        + nom + "', '"
                        + coordonnees + "', "
                        + std::to_string(codePays) + ", "
                        + std::to_string(numeroTel) + ");";
    db.executeQuery(query);

    // La boucle for suivante parcourt tous les éléments du conteneur 'produits'.
    // 'const auto& produit' signifie que 'produit' est une référence constante à chaque élément de 'produits'.
    // Cela permet d'éviter de faire une copie de chaque élément
    // En utilisant 'const', nous garantissons que 'produit' ne sera pas modifié à l'intérieur de la boucle.
    for (const auto& produit : produits) {
        // Construire une requête SQL pour insérer les données du produit dans la table 'FOURNISSEUR_PRODUITS'.
        // 'nom' est le nom du fournisseur et 'produit->getNumeroSerie()' est le numéro de série du produit.
        std::string produitQuery = "INSERT INTO FOURNISSEUR_PRODUITS (FOURNISSEUR_NOM, PRODUIT_NUMEROSERIE) VALUES ('"
                                + nom + "', "
                                + std::to_string(produit->getNumeroSerie()) + ");";
        // Exécuter la requête SQL pour insérer les données dans la base de données.
        db.executeQuery(produitQuery);
    }

}
