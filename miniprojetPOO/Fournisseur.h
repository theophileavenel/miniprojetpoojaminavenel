#ifndef FOURNISSEUR_H
#define FOURNISSEUR_H

#include <iostream>
#include <string>
#include <vector>
#include "Produit.h"
#include "constants.h"
#include "useful.h"
#include "Database.h"

class Fournisseur {
private:
    std::string nom;
    std::string coordonnees;
    int codePays;
    int numeroTel;
    std::vector<Produit*> produits; // Utilisation de vector au lieu de tableau C-style

public:
    Fournisseur(const std::string& nom, const std::string& coord, int codePays, int numeroTel);
    ~Fournisseur();

    void ajouterProduitDepuisSaisie(std::vector<Produit*>& tousLesProduits);  // Méthode pour ajouter un produit
    void ajouterProduit(Produit* produit);
    Produit* trouverProduitParNumeroSerie(const std::vector<Produit*>& tousLesProduits, int numeroSerie);
    void afficher() const;
    static Fournisseur* creerFournisseur();

    std::string getNom() const;
    std::string getCoordonnees() const;
    int getCodePays() const;
    int getNumeroTel() const;
    std::vector<Produit*> getProduitsFournisseur() const; // Changement de type de retour

    // Setters
    void setNom(const std::string& nom);
    void setCoordonnees(const std::string& coordonnees);
    void setCodePays(int codePays);
    void setNumeroTel(int numeroTel);

    friend std::ostream& operator<<(std::ostream& os, const Fournisseur& fournisseur);
    bool operator==(const Fournisseur& other) const;
    bool operator!=(const Fournisseur& autre) const;

    // DATABASE
    static void createTable(Database& db);
    void saveToDatabase(Database& db);
};

#endif // FOURNISSEUR_H
