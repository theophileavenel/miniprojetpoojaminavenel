#include "Personne.h"
#include "Achat.h"

// Implémentation de la classe Personne
Personne::Personne(int datenaissance, long int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
    : datenaissance(datenaissance), numerosecuritesociale(numerosecuritesociale), adressemail(adressemail), nom(nom), prenom(prenom) {}

Personne::~Personne() {
    //std::cout << "Le destructeur va supprimer l'objet de type Personne." << std::endl;
}

std::string Personne::getNom() const {
    return nom;
}

std::string Personne::getPrenom() const{
    return prenom;
}

std::string Personne::getAdresseMail() const{
    return adressemail;
}


long int Personne::getNumerosecuritesociale() const{
    return numerosecuritesociale;
}

int Personne::getDateNaissance() const{
    return datenaissance;
}


void Personne::setNom(const std::string& n) {
    nom = n;
}

void Personne::setPrenom(const std::string& p) {
    prenom = p;
}

void Personne::setAdresseMail(const std::string& email) {
    adressemail = email;
}

void Personne::setNumerosecuritesociale(long int numSecu) {
    numerosecuritesociale = numSecu;
}

void Personne::setDateNaissance(int date) {
    datenaissance = date;
}


// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Personne& personne) {
    os << "Nom: " << personne.nom << "\nPrenom: " << personne.prenom << "\nDate de naissance: " << personne.datenaissance
       << "\nNumero de securite sociale: " << personne.numerosecuritesociale << "\nAdresse email: " << personne.adressemail;
    return os;
}

// Surcharge de l'opérateur ==
bool Personne::operator==(const Personne& autre) const {
    return (datenaissance == autre.datenaissance &&
            numerosecuritesociale == autre.numerosecuritesociale &&
            adressemail == autre.adressemail &&
            nom == autre.nom &&
            prenom == autre.prenom);
}

// Surcharge de l'opérateur !=
bool Personne::operator!=(const Personne& autre) const {
    return !(*this == autre);
}

Personne* Personne::creerPersonne() {
    int choix;
    std::cout << "Choisissez le type de personne:\n";
    std::cout << "1. Personnel\n";
    std::cout << "2. Client\n";
    std::cin >> choix;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Pour ignorer les caractères restants dans le buffer

    int datenaissance, datedebut, datefin;
    long int numerosecuritesociale;
    std::string adressemail, nom, prenom, role;
    double salaire;

    switch (choix) {
        case 1:
            std::cout << "Date de naissance: (année) comprise entre 1850 et 2024 ";
            while (!(std::cin >> datenaissance) || datenaissance < 1850 || datenaissance > 2024) {
                std::cout << "Entrée invalide. Veuillez entrer une année de naissance valide (1850<datenaissance<2024): ";
                std::cin.clear();  // Effacer l'état d'erreur
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Ignorer les caractères restants
            }
            std::cout << std::endl;

            std::cout << "Numéro de sécurité sociale de 13 chiffres ";
            while (!(std::cin >> numerosecuritesociale) || numerosecuritesociale < 1000000000000 || numerosecuritesociale > 9999999999999) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de sécurité sociale valide et composé de 13 chiffres: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cout << std::endl;

            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Pour ignorer les caractères restants dans le buffer

            do {
                std::cout << "Adresse email du Personnel (sans espaces): ";
                std::getline(std::cin, adressemail);
                if (contientEspaces(adressemail) || adressemail.empty()) {
                    std::cout << "Erreur: L'adresse email ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(adressemail) || adressemail.empty());

            do {
                std::cout << "Nom du Personnel (sans espaces): ";
                std::getline(std::cin, nom);
                if (contientEspaces(nom) || nom.empty()) {
                    std::cout << "Erreur: Le nom ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(nom) || nom.empty());

            do {
                std::cout << "Prénom du Personnel (sans espaces): ";
                std::getline(std::cin, prenom);
                if (contientEspaces(prenom) || prenom.empty()) {
                    std::cout << "Erreur: Le prénom ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(prenom) || prenom.empty());

            std::cout << "Salaire: ";
            while (!(std::cin >> salaire) || salaire < 0) {
                std::cout << "Entrée invalide. Veuillez entrer un salaire valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Date de début de travail: (année) comprise entre 1850 et 2030 ";
            while (!(std::cin >> datedebut) || datedebut < 1850 || datedebut > 2030) {
                std::cout << "Entrée invalide. Veuillez entrer une date de début de travail valide (1850<datedebut<2030): ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cout << std::endl;

            std::cout << "Date de fin de travail: (année) comprise entre 1850 et 2030 ";
            while (!(std::cin >> datefin) || datefin < 1850 || datefin > 2030) {
                std::cout << "Entrée invalide. Veuillez entrer une date de fin de travail valide (1850<datefin<2030): ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cout << std::endl;

            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Pour ignorer les caractères restants dans le buffer

            do {
                std::cout << "Rôle du personnel (sans espaces): ";
                std::getline(std::cin, role);
                if (contientEspaces(role) || role.empty()) {
                    std::cout << "Erreur: Le rôle ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(role) || role.empty());
            return new Personnel(datenaissance, numerosecuritesociale, adressemail, nom, prenom, salaire, datedebut, datefin, role);

        case 2:
            std::cout << "Date de naissance: (année) comprise entre 1850 et 2024 ";
            while (!(std::cin >> datenaissance) || datenaissance < 1850 || datenaissance > 2024) {
                std::cout << "Entrée invalide. Veuillez entrer une année de naissance valide (1850<datenaissance<2024): ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cout << std::endl;

            std::cout << "Numéro de sécurité sociale de 13 chiffres ";
            while (!(std::cin >> numerosecuritesociale) || numerosecuritesociale < 1000000000000 || numerosecuritesociale > 9999999999999) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de sécurité sociale valide et composé de 13 chiffres: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cout << std::endl;

            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Pour ignorer les caractères restants dans le buffer

            do {
                std::cout << "Adresse email du client (sans espaces): ";
                std::getline(std::cin, adressemail);
                if (contientEspaces(adressemail) || adressemail.empty()) {
                    std::cout << "Erreur: L'adresse email ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(adressemail) || adressemail.empty());

            do {
                std::cout << "Nom du client (sans espaces): ";
                std::getline(std::cin, nom);
                if (contientEspaces(nom) || nom.empty()) {
                    std::cout << "Erreur: Le nom ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(nom) || nom.empty());

            do {
                std::cout << "Prénom du client (sans espaces): ";
                std::getline(std::cin, prenom);
                if (contientEspaces(prenom) || prenom.empty()) {
                    std::cout << "Erreur: Le prénom ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(prenom) || prenom.empty());
            return new Client(datenaissance, numerosecuritesociale, adressemail, nom, prenom);

        default:
            std::cout << "Choix invalide.\n";
            return nullptr;
    }
}

// Implémentation de la méthode createTable
void Personne::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS PERSONNE ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "NOM TEXT, "
                    "PRENOM TEXT, "
                    "ADRESSEMAIL TEXT, "
                    "DATENAISSANCE INTEGER, "
                    "TYPE INTEGER);");
}

// Implémentation de la méthode saveToDatabase
void Personne::saveToDatabase(Database& db) {
    std::string query = "INSERT INTO PERSONNE (NUMEROSERIE, NOM, PRENOM, ADRESSEMAIL, DATENAISSANCE, TYPE) VALUES ("
                        + std::to_string(numerosecuritesociale) + ", '"
                        + nom + "', '"
                        + prenom + "', '"
                        + adressemail + "', "
                        + std::to_string(datenaissance) + ", "
                        + std::to_string(type) + ");";
    db.executeQuery(query);
}

// Implémentation de la classe Personnel
/*Personnel::Personnel(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
                     double salaire, int datedebut, int datefin, const std::string& role)
    : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), salaire(salaire), datedebut(datedebut), datefin(datefin), role(role) {}*/

Personnel::~Personnel() {
    std::cout << "Le destructeur va supprimer l'objet de type Personnel." << std::endl;
}

void Personnel::afficher() const {
    std::cout << "Bonjour, je suis " << prenom << " " << nom
              << " et je suis né en " << datenaissance // Sa date de naissance (on va utiliser l’année seulement pour ce projet)
              << ". Mon adresse mail est : " << adressemail 
              << ". Je suis membre du personnel depuis l'année " << datedebut 
              << " et ma date de fin de travail est " << datefin 
              << ". Mon rôle est " << role 
              << " et mon salaire est de " << salaire
              << std::endl;
}

double Personnel::getSalaire() const {
    return salaire;
}

int Personnel::getDateDebut() const {
    return datedebut;
}

int Personnel::getDateFin() const {
    return datefin;
}

std::string Personnel::getRole() const {
    return role;
}

bool Personnel::estSousContrat(int dateActuelle) const {
    std::cout << dateActuelle;
    return (dateActuelle >= datedebut && dateActuelle <= datefin);
}


void Personnel::setSalaire(double s) {
    salaire = s;
}

void Personnel::setDateDebut(int debut) {
    datedebut = debut;
}

void Personnel::setDateFin(int fin) {
    datefin = fin;
}

void Personnel::setRole(const std::string& r) {
    role = r;
}

std::ostream& operator<<(std::ostream& os, const Personnel& personnel) {
    os << "Nom: " << personnel.getNom() << "\n"
       << "Prenom: " << personnel.getPrenom() << "\n"
       << "Adresse Email: " << personnel.getAdresseMail() << "\n"
       << "Numero de securite sociale: " << personnel.getNumerosecuritesociale() << "\n"
       << "Date de naissance: " << personnel.getDateNaissance() << "\n"
       << "Salaire: " << personnel.salaire << "\n"
       << "Date de debut: " << personnel.datedebut << "\n"
       << "Date de fin: " << personnel.datefin << "\n"
       << "Role: " << personnel.role;
    return os;
}

bool Personnel::operator==(const Personnel& autre) const {
    return Personne::operator==(autre) && salaire == autre.salaire && datedebut == autre.datedebut && datefin == autre.datefin && role == autre.role;
}

bool Personnel::operator!=(const Personnel& autre) const {
    return !(*this == autre);
}

void Personnel::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS PERSONNEL ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "NOM TEXT, "
                    "PRENOM TEXT, "
                    "ADRESSEMAIL TEXT, "
                    "DATENAISSANCE INTEGER, "
                    "SALAIRE REAL, "
                    "DATEDEBUT INTEGER, "
                    "DATEFIN INTEGER, "
                    "ROLE TEXT);");
}

void Personnel::saveToDatabase(Database& db) {
    std::string query = "INSERT INTO PERSONNEL (NUMEROSERIE, NOM, PRENOM, ADRESSEMAIL, DATENAISSANCE, SALAIRE, DATEDEBUT, DATEFIN, ROLE) VALUES ("
                        + std::to_string(numerosecuritesociale) + ", '"
                        + nom + "', '"
                        + prenom + "', '"
                        + adressemail + "', "
                        + std::to_string(datenaissance) + ", "
                        + std::to_string(salaire) + ", "
                        + std::to_string(datedebut) + ", "
                        + std::to_string(datefin) + ", '"
                        + role + "');";
    db.executeQuery(query);
}

// Constructeur de la classe Client
/*Client::Client(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
    : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), nombreAchats(0) {}*/

Client::~Client() {
    std::cout << "Le destructeur va supprimer l'objet de type Client." << std::endl;
    for (int i = 0; i < nombreAchats; ++i) {
        delete anciensAchats[i];
    }
}

// Méthode pour ajouter un achat dans le tableau des anciens achats
void Client::ajouterAchat(Achat* achat) {
    if (nombreAchats < MAX_ACHATS) {
        anciensAchats[nombreAchats++] = achat;
    } else {
        std::cout << "Liste d'achats pleine, impossible d'ajouter plus d'achats." << std::endl;
    }
}

// Méthode pour afficher les achats du client
void Client::afficherAchats() const {
    std::cout << "Achats de " << nom << " :" << std::endl;
    for (int i = 0; i < nombreAchats; ++i) {
        anciensAchats[i]->afficherDetails();
    }
}

// Méthode pour afficher les informations du client
void Client::afficher() const {
    std::cout << "Bonjour, je suis " << prenom << " " << nom
              << " et je suis né en " << datenaissance
              << ". Mon adresse mail est : " << adressemail;

    if (nombreAchats > 0) {
        std::cout << ". Voici le numéro de mon dernier achat : " 
                  << anciensAchats[nombreAchats - 1]->getNumeroAchat() << std::endl;
    } else {
        std::cout << ". Je n'ai pas encore fait d'achats." << std::endl;
    }
}


std::ostream& operator<<(std::ostream& os, const Client& client) {
    os << "Nom: " << client.getNom() << "\n"
       << "Prenom: " << client.getPrenom() << "\n"
       << "Adresse Email: " << client.getAdresseMail() << "\n"
       << "Numero de securite sociale: " << client.getNumerosecuritesociale() << "\n"
       << "Date de naissance: " << client.getDateNaissance() << "\n"
       << "Nombre d'achats: " << client.nombreAchats;
    for (int i = 0; i < client.nombreAchats; ++i) {
        os << "\nAchat " << i + 1 << ": " << *(client.anciensAchats[i]);
    }
    return os;
}

bool Client::operator==(const Client& autre) const {
    if (Personne::operator==(autre) && nombreAchats == autre.nombreAchats) {
        for (int i = 0; i < nombreAchats; ++i) {
            if (*(anciensAchats[i]) != *(autre.anciensAchats[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

bool Client::operator!=(const Client& autre) const {
    return !(*this == autre);
}

void Client::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS CLIENT ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "NOM TEXT, "
                    "PRENOM TEXT, "
                    "ADRESSEMAIL TEXT, "
                    "DATENAISSANCE INTEGER);");
}

void Client::saveToDatabase(Database& db) {
    std::string query = "INSERT INTO CLIENT (NUMEROSERIE, NOM, PRENOM, ADRESSEMAIL, DATENAISSANCE) VALUES ("
                        + std::to_string(numerosecuritesociale) + ", '"
                        + nom + "', '"
                        + prenom + "', '"
                        + adressemail + "', "
                        + std::to_string(datenaissance) + ");";
    db.executeQuery(query);
}