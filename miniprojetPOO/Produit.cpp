#include "Produit.h"

// Implémentation de la classe Produit
Produit::Produit(int numeroserie, int quantite, double prixunitaire)
    : numeroserie(numeroserie), quantite(quantite), prixunitaire(prixunitaire) {}

Produit::~Produit() {
    //std::cout << "Le destructeur va supprimer l'objet de type Produit." << std::endl;
}

void Produit::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS PRODUIT ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "QUANTITE INTEGER, "
                    "PRIX REAL, "
                    "TYPE INTEGER);");
}

/*
void Produit::saveToDatabase(Database& db) const {
    std::string query = "INSERT INTO PRODUIT (NUMEROSERIE, QUANTITE, PRIX, TYPE) VALUES ("
                        + std::to_string(numeroserie) + ", "
                        + std::to_string(quantite) + ", "
                        + std::to_string(prixunitaire) + ", "
                        + std::to_string(typeproduit) + ");";
    db.executeQuery(query);
}*/

double Produit::getPrix() const {
    return prixunitaire;
}

int Produit::getQuantite() const {
    return quantite;
}

int Produit::getNumeroSerie() const {
    return numeroserie;
}

void Produit::setPrix(double prix) {
    prixunitaire = prix;
}

void Produit::setQuantite(int quant) {
    quantite = quant;
}

void Produit::setNumeroSerie(int numSerie) {
    numeroserie = numSerie;
}

// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Produit& produit) {
    os << "Numero de serie: " << produit.numeroserie << "\nQuantite: " << produit.quantite << "\nPrix unitaire: " << produit.prixunitaire;
    return os;
}

// Surcharge de l'opérateur ==
bool Produit::operator==(const Produit& autre) const {
    return (numeroserie == autre.numeroserie &&
            quantite == autre.quantite &&
            prixunitaire == autre.prixunitaire);
}

// Surcharge de l'opérateur !=
bool Produit::operator!=(const Produit& autre) const {
    return !(*this == autre);
}

Produit* Produit::loadFromDatabase(Database& db, int numeroSerie) {
    std::string query;
    sqlite3_stmt* stmt;

    // Vérification dans la table des produits alimentaires
    query = "SELECT NUMEROSERIE, QUANTITE, PRIX, DATE_PEREMPTION FROM PRODUITALIMENTAIRE WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            sqlite3_finalize(stmt);
            return ProduitAlimentaire::loadFromDatabase(db, numeroSerie);
        }
    }
    sqlite3_finalize(stmt);

    // Vérification dans la table des produits électriques
    query = "SELECT NUMEROSERIE, QUANTITE, PRIX, PUISSANCE, DUREEGARANTIE FROM PRODUITELECTRIQUE WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            sqlite3_finalize(stmt);
            return ProduitElectrique::loadFromDatabase(db, numeroSerie);
        }
    }
    sqlite3_finalize(stmt);

    // Vérification dans la table des jeux
    query = "SELECT NUMEROSERIE, QUANTITE, PRIX, AGEMINIMUM FROM JEU WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            sqlite3_finalize(stmt);
            return Jeu::loadFromDatabase(db, numeroSerie);
        }
    }
    sqlite3_finalize(stmt);

    // Vérification dans la table des logiciels
    query = "SELECT NUMEROSERIE, QUANTITE, PRIX, SYSTEMEEXPLOITATION, CARACTERISTIQUESNECESSAIRES, CATEGORIE FROM LOGICIEL WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            sqlite3_finalize(stmt);
            return Logiciel::loadFromDatabase(db, numeroSerie);
        }
    }
    sqlite3_finalize(stmt);

    std::cout << "Le produit avec le numéro de série " << numeroSerie << " n'existe pas.\n";
    return nullptr;
}

// Implémentation de la classe ProduitAlimentaire
ProduitAlimentaire::ProduitAlimentaire(int numeroserie, int quantite, double prixunitaire, const int datePeremption)
    : Produit(numeroserie, quantite, prixunitaire), datePeremption(datePeremption) {}

ProduitAlimentaire::~ProduitAlimentaire() {
    std::cout << "Le destructeur va supprimer l'objet de type ProduitAlimentaire." << std::endl;
}

void ProduitAlimentaire::afficher() const {
    std::cout << "Produit Alimentaire - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Date de péremption: " << datePeremption << std::endl;
}

int ProduitAlimentaire::getDatePeremption() const{
    return datePeremption;
}

void ProduitAlimentaire::setDatePeremption(int date) {
    datePeremption = date;
}


// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const ProduitAlimentaire& produit) {
    os << "Produit Alimentaire - Numero de serie: " << produit.numeroserie 
       << "\nQuantite: " << produit.quantite 
       << "\nPrix unitaire: " << produit.prixunitaire 
       << "\nDate de péremption: " << produit.datePeremption;
    return os;
}

// Surcharge de l'opérateur ==
bool ProduitAlimentaire::operator==(const ProduitAlimentaire& autre) const {
    return Produit::operator==(autre) && datePeremption == autre.datePeremption;
}

// Surcharge de l'opérateur !=
bool ProduitAlimentaire::operator!=(const ProduitAlimentaire& autre) const {
    return !(*this == autre);
}
void ProduitAlimentaire::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS PRODUITALIMENTAIRE ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "QUANTITE INTEGER, "
                    "PRIX REAL, "
                    "DATE_PEREMPTION INTEGER);");
}


void ProduitAlimentaire::saveToDatabase(Database& db) const {
    std::string query = "INSERT INTO PRODUITALIMENTAIRE (NUMEROSERIE, QUANTITE, PRIX, DATE_PEREMPTION) VALUES ("
                        + std::to_string(numeroserie) + ", "
                        + std::to_string(quantite) + ", "
                        + std::to_string(prixunitaire) + ", "
                        + std::to_string(datePeremption) + ");";
    db.executeQuery(query);
}

Produit* ProduitAlimentaire::loadFromDatabase(Database& db, int numeroSerie) {
    std::string query = "SELECT NUMEROSERIE, QUANTITE, PRIX, DATE_PEREMPTION FROM PRODUITALIMENTAIRE WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            int numeroSerie = sqlite3_column_int(stmt, 0);
            int quantite = sqlite3_column_int(stmt, 1);
            double prix = sqlite3_column_double(stmt, 2);
            int datePeremption = sqlite3_column_int(stmt, 3);
            sqlite3_finalize(stmt);
            return new ProduitAlimentaire(numeroSerie, quantite, prix, datePeremption);
        }
    }
    sqlite3_finalize(stmt);
    std::cerr << "Produit alimentaire introuvable\n";
    return nullptr;
}

void ProduitAlimentaire::deleteFromDatabase(Database& db, int numeroSerie) {
    std::string sql = "DELETE FROM PRODUITALIMENTAIRE WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    db.executeSQL(sql);
}



// Implémentation de la classe ProduitElectrique
ProduitElectrique::ProduitElectrique(int numeroserie, int quantite, double prixunitaire, double puissance, int dureeGarantie)
    : Produit(numeroserie, quantite, prixunitaire), puissance(puissance), dureeGarantie(dureeGarantie) {}

ProduitElectrique::~ProduitElectrique() {
    std::cout << "Le destructeur va supprimer l'objet de type ProduitElectrique." << std::endl;
}

void ProduitElectrique::afficher() const {
    std::cout << "Produit Electrique - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Puissance: " << puissance 
              << "W, Durée de garantie: " << dureeGarantie << " ans" << std::endl;
}


double ProduitElectrique::getPuissance() const {
    return puissance;
}

int ProduitElectrique::getDureeGarantie() const {
    return dureeGarantie;
}

void ProduitElectrique::setPuissance(double p) {
    puissance = p;
}

void ProduitElectrique::setDureeGarantie(int garantie) {
    dureeGarantie = garantie;
}

Produit* ProduitElectrique::loadFromDatabase(Database& db, int numeroSerie) {
    std::string query = "SELECT NUMEROSERIE, QUANTITE, PRIX, PUISSANCE, DUREEGARANTIE FROM PRODUITELECTRIQUE WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            int numeroSerie = sqlite3_column_int(stmt, 0);
            int quantite = sqlite3_column_int(stmt, 1);
            double prix = sqlite3_column_double(stmt, 2);
            double puissance = sqlite3_column_double(stmt, 3);
            int dureeGarantie = sqlite3_column_int(stmt, 4);
            sqlite3_finalize(stmt);
            return new ProduitElectrique(numeroSerie, quantite, prix, puissance, dureeGarantie);
        }
    }
    sqlite3_finalize(stmt);
    std::cerr << "Produit électrique introuvable\n";
    return nullptr;
}

// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const ProduitElectrique& produit) {
    os << "Produit Electrique - Numero de serie: " << produit.numeroserie 
       << "\nQuantite: " << produit.quantite 
       << "\nPrix unitaire: " << produit.prixunitaire 
       << "\nPuissance: " << produit.puissance 
       << "\nDuree de garantie: " << produit.dureeGarantie;
    return os;
}

// Surcharge de l'opérateur ==
bool ProduitElectrique::operator==(const ProduitElectrique& autre) const {
    return Produit::operator==(autre) && puissance == autre.puissance && dureeGarantie == autre.dureeGarantie;
}

// Surcharge de l'opérateur !=
bool ProduitElectrique::operator!=(const ProduitElectrique& autre) const {
    return !(*this == autre);
}

void ProduitElectrique::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS PRODUITELECTRIQUE ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "QUANTITE INTEGER, "
                    "PRIX REAL, "
                    "PUISSANCE REAL, "
                    "DUREEGARANTIE INTEGER);");
}

void ProduitElectrique::saveToDatabase(Database& db) const {
    std::string query = "INSERT INTO PRODUITELECTRIQUE (NUMEROSERIE, QUANTITE, PRIX, PUISSANCE, DUREEGARANTIE) VALUES ("
                        + std::to_string(numeroserie) + ", "
                        + std::to_string(quantite) + ", "
                        + std::to_string(prixunitaire) + ", "
                        + std::to_string(puissance) + ", "
                        + std::to_string(dureeGarantie) + ");";
    db.executeQuery(query);
}

void ProduitElectrique::deleteFromDatabase(Database& db, int numeroSerie) {
    std::string sql = "DELETE FROM PRODUITELECTRIQUE WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    db.executeSQL(sql);
}


// Implémentation de la classe Jeu
Jeu::Jeu(int numeroserie, int quantite, double prixunitaire, int ageMinimum)
    : Produit(numeroserie, quantite, prixunitaire), ageMinimum(ageMinimum) {}

Jeu::~Jeu() {
    std::cout << "Le destructeur va supprimer l'objet de type Jeu." << std::endl;
}

void Jeu::afficher() const {
    std::cout << "Jeu - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Age minimum: " << ageMinimum << " ans" << std::endl;
}

int Jeu::getAgeMinimum() const{
    return ageMinimum;
}


void Jeu::setAgeMinimum(int age) {
    ageMinimum = age;
}

void Jeu::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS JEU ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "QUANTITE INTEGER, "
                    "PRIX REAL, "
                    "AGEMINIMUM INTEGER);");
}

void Jeu::saveToDatabase(Database& db) const {
    std::string sql = "INSERT INTO JEU (NUMEROSERIE, QUANTITE, PRIX, AGEMINIMUM) VALUES (" 
                      + std::to_string(numeroserie) + ", " 
                      + std::to_string(quantite) + ", " 
                      + std::to_string(prixunitaire) + ", "
                      + std::to_string(ageMinimum) + ");";
    db.executeSQL(sql);
}

Produit* Jeu::loadFromDatabase(Database& db, int numeroSerie) {
    std::string query = "SELECT NUMEROSERIE, QUANTITE, PRIX, AGEMINIMUM FROM JEU WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            int numeroSerie = sqlite3_column_int(stmt, 0);
            int quantite = sqlite3_column_int(stmt, 1);
            double prix = sqlite3_column_double(stmt, 2);
            int ageMinimum = sqlite3_column_int(stmt, 3);
            sqlite3_finalize(stmt);
            return new Jeu(numeroSerie, quantite, prix, ageMinimum);
        }
    }
    sqlite3_finalize(stmt);
    std::cerr << "Jeu introuvable\n";
    return nullptr;
}

void Jeu::deleteFromDatabase(Database& db, int numeroSerie) {
    std::string sql = "DELETE FROM JEU WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    db.executeSQL(sql);
}



// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Jeu& jeu) {
    os << "Jeu - Numero de serie: " << jeu.numeroserie 
       << "\nQuantite: " << jeu.quantite 
       << "\nPrix unitaire: " << jeu.prixunitaire 
       << "\nAge minimum: " << jeu.ageMinimum;
    return os;
}

// Surcharge de l'opérateur ==
bool Jeu::operator==(const Jeu& autre) const {
    return Produit::operator==(autre) && ageMinimum == autre.ageMinimum;
}

// Surcharge de l'opérateur !=
bool Jeu::operator!=(const Jeu& autre) const {
    return !(*this == autre);
}

// Implémentation de la classe Logiciel
Logiciel::Logiciel(int numeroserie, int quantite, double prixunitaire, const std::string& systemeExploitation, const std::string& caracteristiquesNecessaires, const std::string& categorie)
    : Produit(numeroserie, quantite, prixunitaire), systemeExploitation(systemeExploitation), caracteristiquesNecessaires(caracteristiquesNecessaires), categorie(categorie) {}

Logiciel::~Logiciel() {
    std::cout << "Le destructeur va supprimer l'objet de type Logiciel." << std::endl;
}

void Logiciel::afficher() const {
    std::cout << "Logiciel - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Système d'exploitation: " << systemeExploitation 
              << ", Caractéristiques nécessaires: " << caracteristiquesNecessaires 
              << ", Catégorie: " << categorie << std::endl;
}

std::string Logiciel::getSystemeExploitation() const {
    return systemeExploitation;
}

std::string Logiciel::getCaracteristiquesNecessaires() const{
    return caracteristiquesNecessaires;
}

std::string Logiciel::getCategorie() const{
    return categorie;
}

void Logiciel::setSystemeExploitation(const std::string& systeme) {
    systemeExploitation = systeme;
}

void Logiciel::setCaracteristiquesNecessaires(const std::string& caracteristiques) {
    caracteristiquesNecessaires = caracteristiques;
}

void Logiciel::setCategorie(const std::string& cat) {
    categorie = cat;
}

// Surcharge de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Logiciel& logiciel) {
    os << "Logiciel - Numero de serie: " << logiciel.numeroserie 
       << "\nQuantite: " << logiciel.quantite 
       << "\nPrix unitaire: " << logiciel.prixunitaire 
       << "\nSysteme d'exploitation: " << logiciel.systemeExploitation 
       << "\nCaracteristiques necessaires: " << logiciel.caracteristiquesNecessaires 
       << "\nCategorie: " << logiciel.categorie;
    return os;
}

// Surcharge de l'opérateur ==
bool Logiciel::operator==(const Logiciel& autre) const {
    return Produit::operator==(autre) && systemeExploitation == autre.systemeExploitation &&
           caracteristiquesNecessaires == autre.caracteristiquesNecessaires && categorie == autre.categorie;
}

// Surcharge de l'opérateur !=
bool Logiciel::operator!=(const Logiciel& autre) const {
    return !(*this == autre);
}

void Logiciel::createTable(Database& db) {
    db.executeQuery("CREATE TABLE IF NOT EXISTS LOGICIEL ("
                    "NUMEROSERIE INTEGER PRIMARY KEY, "
                    "QUANTITE INTEGER, "
                    "PRIX REAL, "
                    "SYSTEMEEXPLOITATION TEXT, "
                    "CARACTERISTIQUESNECESSAIRES TEXT, "
                    "CATEGORIE TEXT);");
}

void Logiciel::saveToDatabase(Database& db) const {
    std::string sql = "INSERT INTO LOGICIEL (NUMEROSERIE, QUANTITE, PRIX, SYSTEMEEXPLOITATION, CARACTERISTIQUESNECESSAIRES, CATEGORIE) VALUES (" 
                        + std::to_string(numeroserie) + ", " 
                        + std::to_string(quantite) + ", " 
                        + std::to_string(prixunitaire) + ", '"
                        + systemeExploitation + "', '"
                        + caracteristiquesNecessaires + "', '"
                        + categorie + "');";
    db.executeSQL(sql);
}

void Logiciel::deleteFromDatabase(Database& db, int numeroSerie) {
    std::string sql = "DELETE FROM LOGICIEL WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    db.executeSQL(sql);
}


Produit* Logiciel::loadFromDatabase(Database& db, int numeroSerie) {
    std::string query = "SELECT NUMEROSERIE, QUANTITE, PRIX, SYSTEMEEXPLOITATION, CARACTERISTIQUESNECESSAIRES, CATEGORIE FROM LOGICIEL WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(db.getDB(), query.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            int numeroSerie = sqlite3_column_int(stmt, 0);
            int quantite = sqlite3_column_int(stmt, 1);
            double prix = sqlite3_column_double(stmt, 2);
            std::string systemeExploitation = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 3));
            std::string caracteristiquesNecessaires = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 4));
            std::string categorie = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 5));
            sqlite3_finalize(stmt);
            return new Logiciel(numeroSerie, quantite, prix, systemeExploitation, caracteristiquesNecessaires, categorie);
        }
    }
    sqlite3_finalize(stmt);
    std::cerr << "Logiciel introuvable\n";
    return nullptr;
}


Produit* Produit::creerProduit() {
    int choix;
    std::cout << "Choisissez le type de produit:\n";
    std::cout << "1. Produit Alimentaire\n";
    std::cout << "2. Produit Electrique\n";
    std::cout << "3. Jeu\n";
    std::cout << "4. Logiciel\n";
    std::cin >> choix;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Pour ignorer les caractères restants dans le buffer

    int numero, quantite, datePeremption, puissance, dureeGarantie, ageMinimum;
    double prix;
    std::string systemeExploitation, caracteristiquesNecessaires, categorie;

    switch (choix) {
        case 1:
            std::cout << "Numéro de série: ";
            while (!(std::cin >> numero)) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de série valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Quantité: ";
            while (!(std::cin >> quantite) || quantite < 0) {
                std::cout << "Entrée invalide. Veuillez entrer une quantité valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Prix unitaire: ";
            while (!(std::cin >> prix) || prix < 0) {
                std::cout << "Entrée invalide. Veuillez entrer un prix valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Date de péremption: ";
            while (!(std::cin >> datePeremption)) {
                std::cout << "Entrée invalide. Veuillez entrer une date de péremption valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            return new ProduitAlimentaire(numero, quantite, prix, datePeremption);

        case 2:
            std::cout << "Numéro de série: ";
            while (!(std::cin >> numero)) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de série valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Quantité: ";
            while (!(std::cin >> quantite) || quantite < 0) {
                std::cout << "Entrée invalide. Veuillez entrer une quantité valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Prix unitaire: ";
            while (!(std::cin >> prix) || prix < 0) {
                std::cout << "Entrée invalide. Veuillez entrer un prix valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Puissance: ";
            while (!(std::cin >> puissance) || puissance < 0) {
                std::cout << "Entrée invalide. Veuillez entrer une puissance valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Durée de garantie: ";
            while (!(std::cin >> dureeGarantie) || dureeGarantie < 0) {
                std::cout << "Entrée invalide. Veuillez entrer une durée de garantie valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            return new ProduitElectrique(numero, quantite, prix, puissance, dureeGarantie);

        case 3:
            std::cout << "Numéro de série: ";
            while (!(std::cin >> numero)) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de série valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Quantité: ";
            while (!(std::cin >> quantite) || quantite < 0) {
                std::cout << "Entrée invalide. Veuillez entrer une quantité valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Prix unitaire: ";
            while (!(std::cin >> prix) || prix < 0) {
                std::cout << "Entrée invalide. Veuillez entrer un prix valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Age minimum: ";
            while (!(std::cin >> ageMinimum) || ageMinimum < 0) {
                std::cout << "Entrée invalide. Veuillez entrer un âge minimum valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            return new Jeu(numero, quantite, prix, ageMinimum);

        case 4:
            std::cout << "Numéro de série: ";
            while (!(std::cin >> numero)) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de série valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Quantité: ";
            while (!(std::cin >> quantite) || quantite < 0) {
                std::cout << "Entrée invalide. Veuillez entrer une quantité valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cout << "Prix unitaire: ";
            while (!(std::cin >> prix) || prix < 0) {
                std::cout << "Entrée invalide. Veuillez entrer un prix valide: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Pour ignorer les caractères restants dans le buffer

            do {
                std::cout << "Système d'exploitation (sans espaces): ";
                std::getline(std::cin, systemeExploitation);

                if (contientEspaces(systemeExploitation) || systemeExploitation.empty()) {
                    std::cout << "Erreur: Le système d'exploitation ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(systemeExploitation) || systemeExploitation.empty());

            do {
                std::cout << "Caractéristiques nécessaires (sans espaces): ";
                std::getline(std::cin, caracteristiquesNecessaires);

                if (contientEspaces(caracteristiquesNecessaires) || caracteristiquesNecessaires.empty()) {
                    std::cout << "Erreur: Les caractéristiques nécessaires ne doivent pas contenir d'espaces et ne doivent pas être vides. Veuillez réessayer.\n";
                }
            } while (contientEspaces(caracteristiquesNecessaires) || caracteristiquesNecessaires.empty());

            do {
                std::cout << "Catégorie (sans espaces): ";
                std::getline(std::cin, categorie);

                if (contientEspaces(categorie) || categorie.empty()) {
                    std::cout << "Erreur: La catégorie ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
                }
            } while (contientEspaces(categorie) || categorie.empty());

            return new Logiciel(numero, quantite, prix, systemeExploitation, caracteristiquesNecessaires, categorie);

        default:
            std::cout << "Choix invalide.\n";
            return nullptr;
    }
}

