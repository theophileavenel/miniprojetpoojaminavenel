#ifndef PRODUIT_H
#define PRODUIT_H

#include <iostream>
#include <string>
#include <vector>
#include "useful.h"
#include "Database.h"
#include <limits>

// Classe abstraite Produit
class Produit {
protected:
    int numeroserie;
    int quantite;
    double prixunitaire;
    TypeProduit typeproduit;

public:
    Produit(int numeroserie, int quantite, double prixunitaire);

    virtual void afficher() const = 0; // Méthode pure virtuelle pour définir la classe abstraite

    static Produit* creerProduit();

    virtual ~Produit();

    double getPrix() const;
    int getQuantite() const;
    int getNumeroSerie() const; // Déclaration de la méthode getNumeroSerie

    // Setters
    void setPrix(double prixunitaire);
    void setQuantite(int quantite);
    void setNumeroSerie(int numeroserie);

    // Surcharges d'opérateurs
    friend std::ostream& operator<<(std::ostream& os, const Produit& produit);
    bool operator==(const Produit& autre) const;
    bool operator!=(const Produit& autre) const;

    // DATABASE
    static void createTable(Database& db);
    virtual void saveToDatabase(Database& db) const = 0; // Méthode virtuelle pure
    static Produit* loadFromDatabase(Database& db, int numeroSerie);
};

// Produits alimentaires
class ProduitAlimentaire : public Produit {
private:
    int datePeremption; // Année de péremption

public:
    ProduitAlimentaire(int numeroserie, int quantite, double prixunitaire, const int datePeremption);

    ~ProduitAlimentaire();

    int getDatePeremption() const;

    void afficher() const;

    // Setters
    void setDatePeremption(int datePeremption);

    // Surcharges d'opérateurs
    friend std::ostream& operator<<(std::ostream& os, const ProduitAlimentaire& produit);
    bool operator==(const ProduitAlimentaire& autre) const;
    bool operator!=(const ProduitAlimentaire& autre) const;

    // DATABASE
    static void createTable(Database& db);
    void saveToDatabase(Database& db) const;
    static Produit* loadFromDatabase(Database& db, int numeroSerie);
    static void deleteFromDatabase(Database& db, int numeroSerie);
};

// Produits électriques
class ProduitElectrique : public Produit {
private:
    double puissance;
    int dureeGarantie; // Durée de la garantie s'exprime en année

public:
    ProduitElectrique(int numeroserie, int quantite, double prixunitaire, double puissance, int dureeGarantie);

    ~ProduitElectrique();

    double getPuissance() const;

    int getDureeGarantie() const;

    void afficher() const;

    // Setters
    void setPuissance(double puissance);
    void setDureeGarantie(int dureeGarantie);

    // Surcharges d'opérateurs
    friend std::ostream& operator<<(std::ostream& os, const ProduitElectrique& produit);
    bool operator==(const ProduitElectrique& autre) const;
    bool operator!=(const ProduitElectrique& autre) const;

    // DATABASE
    static void createTable(Database& db);
    void saveToDatabase(Database& db) const;
    static Produit* loadFromDatabase(Database& db, int numeroSerie);
    static void deleteFromDatabase(Database& db, int numeroSerie);
};

// Jeux
class Jeu : public Produit {
private:
    int ageMinimum;

public:
    Jeu(int numeroserie, int quantite, double prixunitaire, int ageMinimum);

    ~Jeu();

    int getAgeMinimum() const;

    void afficher() const;

    // Setters
    void setAgeMinimum(int ageMinimum);

    // Surcharges d'opérateurs
    friend std::ostream& operator<<(std::ostream& os, const Jeu& jeu);
    bool operator==(const Jeu& autre) const;
    bool operator!=(const Jeu& autre) const;

    // DATABASE
    static void createTable(Database& db);
    void saveToDatabase(Database& db) const;
    static Produit* loadFromDatabase(Database& db, int numeroSerie);
    static void deleteFromDatabase(Database& db, int numeroSerie);
};

// Logiciels
class Logiciel : public Produit {
private:
    std::string systemeExploitation;
    std::string caracteristiquesNecessaires;
    std::string categorie;

public:
    Logiciel(int numeroserie, int quantite, double prixunitaire, const std::string& systemeExploitation, const std::string& caracteristiquesNecessaires, const std::string& categorie);

    ~Logiciel();

    void afficher() const;
    std::string getSystemeExploitation() const;
    std::string getCaracteristiquesNecessaires() const;
    std::string getCategorie() const;

    // Setters
    void setSystemeExploitation(const std::string& systemeExploitation);
    void setCaracteristiquesNecessaires(const std::string& caracteristiquesNecessaires);
    void setCategorie(const std::string& categorie);

    // Surcharges d'opérateurs
    friend std::ostream& operator<<(std::ostream& os, const Logiciel& logiciel);
    bool operator==(const Logiciel& autre) const;
    bool operator!=(const Logiciel& autre) const;

    // DATABASE
    static void createTable(Database& db);
    void saveToDatabase(Database& db) const;
    static Produit* loadFromDatabase(Database& db, int numeroSerie);
    static void deleteFromDatabase(Database& db, int numeroSerie);
};

#endif // PRODUIT_H
