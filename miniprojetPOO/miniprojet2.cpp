#include <iostream>
#include "Fournisseur.h"
#include "Personne.h"
#include "Produit.h"
#include "Achat.h"
#include "constants.h"
#include "useful.h"
#include "Database.h"

//g++ -g miniprojet2.cpp Database.cpp Produit.cpp Personne.cpp Fournisseur.cpp Achat.cpp useful.cpp -o miniprojet2 -lsqlite3

//gdb ./miniprojet2

//run

//exit

std::vector<Produit*> produits; 
Personne* personnes[MAX_PERSONNES];
Fournisseur* fournisseurs[MAX_FOURNISSEURS];
Achat* achats[MAX_ACHATS];

int nbProduits = 0;
int nbPersonnes = 0;
int nbPersonnels = 0;
int nbClients= 0;
int nbFournisseurs = 0;
int nbAchats = 0;

void afficherMenu() {
    std::cout << "\nMenu:\n";
    std::cout << "1. Créer un produit\n";
    std::cout << "2. Créer une personne\n";
    std::cout << "3. Créer un fournisseur\n";
    std::cout << "4. Créer un achat\n";
    std::cout << "5. Afficher les produits d'un fournisseur\n";
    std::cout << "6. Afficher les achats d'un client\n";
    std::cout << "7. Afficher les personnels sous contrat\n";
    std::cout << "8. Afficher les personnels par salaire\n";
    std::cout << "9. Afficher les produits par prix unitaire\n";
    std::cout << "10. Calculer la valeur du stock\n";
    std::cout << "11. Ajouter un produit à un fournisseur\n";
    std::cout << "12. Interaction avec la base de données SQLITE\n";
    std::cout << "0. Quitter\n";
}

void afficherFournisseursProduits() {
    std::string nom;
    std::cout << "Nom du fournisseur: ";
    std::cin >> nom;
    for (int i = 0; i < nbFournisseurs; ++i) {
        if (fournisseurs[i]->getNom() == nom) {
            fournisseurs[i]->afficher();
            return;
        }
    }
    std::cout << "Fournisseur non trouvé.\n";
}

void afficherAchatsClient() {
    std::string nom;
    std::cout << "Nom du client: ";
    std::cin >> nom;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getType() == CLIENT && personnes[i]->getNom() == nom) {
            personnes[i]->afficher();
            Client* client = dynamic_cast<Client*>(personnes[i]);
            if (client) {
                client->afficherAchats();
            }
            return;
        }
    }
    std::cout << "Client non trouvé.\n";
}

/*
void afficherPersonnelsSousContrat() {
    int dateActuelle;
    std::cout << "Entrez l'année actuelle: ";
    std::cin >> dateActuelle;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getType() == PERSONNEL && personnes[i]->estSousContrat(dateActuelle)) {
            personnes[i]->afficher();
        }
    }
}*/

void afficherPersonnelsSousContrat() {
    int dateActuelle;
    std::cout << "Entrez l'année actuelle: ";
    std::cin >> dateActuelle;
    for (int i = 0; i < nbPersonnes; ++i) {
        Personnel* personnel = dynamic_cast<Personnel*>(personnes[i]);
        if (personnel && personnel->estSousContrat(dateActuelle)) {
            personnel->afficher();
        }
    }
}

/*
void afficherPersonnelsParSalaire() {
    double salaireMin, salaireMax;
    std::cout << "Salaire minimum: ";
    std::cin >> salaireMin;
    std::cout << "Salaire maximum: ";
    std::cin >> salaireMax;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getType() == PERSONNEL ) {
            personnes[i]->afficher();
        }
    }
}*/

void afficherPersonnelsParSalaire() {
    double salaireMin, salaireMax;
    std::cout << "Salaire minimum: ";
    std::cin >> salaireMin;
    std::cout << "Salaire maximum: ";
    std::cin >> salaireMax;
    for (int i = 0; i < nbPersonnes; ++i) {
        Personnel* personnel = dynamic_cast<Personnel*>(personnes[i]);
        if (personnel && personnel->getSalaire() >= salaireMin && personnel->getSalaire() <= salaireMax) {
            personnel->afficher();
        }
    }
}


void afficherProduitsParPrix() {
    double prixMin, prixMax;
    std::cout << "Prix minimum: ";
    std::cin >> prixMin;
    std::cout << "Prix maximum: ";
    std::cin >> prixMax;
    for (Produit* produit : produits) {
        if (produit->getPrix() >= prixMin && produit->getPrix() <= prixMax) {
            produit->afficher();
        }
    }
}

double calculerValeurStock() {
    double valeurTotale = 0.0;
    for (Produit* produit : produits) {
        valeurTotale += produit->getPrix() * produit->getQuantite();
    }
    return valeurTotale;
}

void ajouterProduitAchat(Achat* achat) {
    int choixProduit;
    std::cout << "Entrez le numéro de série du produit à ajouter à l'achat: ";
    std::cin >> choixProduit;
    for (Produit* produit : produits) {
        if (produit->getNumeroSerie() == choixProduit) {
            achat->ajouterProduitAchat(produit);
            std::cout << "Produit ajouté à l'achat.\n";
            return;
        }
    }
    std::cout << "Produit non trouvé.\n";
}


void ajouterPersonneAchat(Achat* achat) {
    std::string nomPersonne;
    std::cout << "Entrez le nom de la personne à ajouter à l'achat: ";
    std::cin >> nomPersonne;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getNom() == nomPersonne) {
            achat->ajouterPersonneAchat(personnes[i]);
            std::cout << (personnes[i]->getType() == CLIENT ? "Client" : "Personnel") << " ajouté à l'achat.\n";
            return;
        }
    }
    std::cout << "Personne non trouvée.\n";
}


// Fonction pour afficher tous les produits de différentes tables
void displayAllProducts(Database& db) {
    std::cout << "Produits Alimentaires:" << std::endl;
    db.executeSQL("SELECT * FROM PRODUITALIMENTAIRE;");
    std::cout << "Produits Electriques:" << std::endl;
    db.executeSQL("SELECT * FROM PRODUITELECTRIQUE;");
    std::cout << "Jeux:" << std::endl;
    db.executeSQL("SELECT * FROM JEU;");
    std::cout << "Logiciels:" << std::endl;
    db.executeSQL("SELECT * FROM LOGICIEL;");
}


void afficherBienvenue() {
    std::cout << "---------------------------------\n";
    std::cout << "| Bienvenue au Magasin          |\n";
    std::cout << "|                               |\n";
    std::cout << "|   M     M     A     GGGGG     |\n";
    std::cout << "|   MM   MM    A A    G         |\n";
    std::cout << "|   M M M M   A   A   G  GG     |\n";
    std::cout << "|   M  M  M  AAAAAAA  G   G     |\n";
    std::cout << "|   M     M A       A  GGGG     |\n";
    std::cout << "|                               |\n";
    std::cout << "---------------------------------\n";
}
void interagirAvecBaseDeDonnees() {
    Database db("entreprise.db");
    int choixDB;
    do {
        std::cout << "\nMenu SQLite:\n";
        std::cout << "1. Créer les tables\n";
        std::cout << "2. Sauvegarder un produit dans la base de données\n";
        std::cout << "3. Sauvegarder une personne dans la base de données\n";
        std::cout << "4. Sauvegarder un fournisseur dans la base de données\n";
        std::cout << "5. Sauvegarder un achat dans la base de données\n";
        std::cout << "6. Afficher les produits depuis la base de données\n";
        std::cout << "7. Afficher les personnes depuis la base de données\n";
        std::cout << "8. Afficher les fournisseurs depuis la base de données\n";
        std::cout << "9. Afficher les achats depuis la base de données\n";
        std::cout << "10. Supprimer un produit de la base de données\n";
        std::cout << "11. Supprimer une personne de la base de données\n";
        std::cout << "12. Supprimer un fournisseur de la base de données\n";
        std::cout << "13. Supprimer un achat de la base de données\n";
        std::cout << "14. Charger un produit depuis la base de données\n";
        std::cout << "0. Retour au menu principal\n";
        std::cin >> choixDB;

        switch (choixDB) {
            case 1:
                ProduitAlimentaire::createTable(db);
                ProduitElectrique::createTable(db);
                Jeu::createTable(db);
                Logiciel::createTable(db);
                Personne::createTable(db);
                Fournisseur::createTable(db);
                Achat::createTable(db);
                std::cout << "Tables créées avec succès.\n";
                break;
            case 2: {
                Produit* produit = Produit::creerProduit();
                if (produit) {
                    produit->saveToDatabase(db);
                    std::cout << "Produit sauvegardé avec succès.\n";
                } else {
                    std::cout << "Erreur lors de la création du produit.\n";
                }
                break;
            }
            case 3: {
                Personne* personne = Personne::creerPersonne();
                if (personne) {
                    personne->saveToDatabase(db);
                    std::cout << "Personne sauvegardée avec succès.\n";
                }
                break;
            }
            case 4: {
                Fournisseur* fournisseur = Fournisseur::creerFournisseur();
                if (fournisseur) {
                    fournisseur->saveToDatabase(db);
                    std::cout << "Fournisseur sauvegardé avec succès.\n";
                }
                break;
            }
            case 5: {
                int date;
                std::cout << "Date de l'achat (année): ";
                std::cin >> date;
                Achat* achat = new Achat(date);
                achat->saveToDatabase(db);
                std::cout << "Achat sauvegardé avec succès.\n";
                break;
            }
            case 6:
                displayAllProducts(db);
                break;
            case 7:
                db.executeSQL("SELECT * FROM PERSONNE;");
                break;
            case 8:
                db.executeSQL("SELECT * FROM FOURNISSEUR;");
                break;
            case 9:
                db.executeSQL("SELECT * FROM ACHAT;");
                break;
            /*case 10: {
                int numeroSerie;
                std::cout << "Entrez le numéro de série du produit à supprimer: ";
                std::cin >> numeroSerie;
                std::string sql = "DELETE FROM PRODUIT WHERE NUMEROSERIE = " + std::to_string(numeroSerie) + ";";
                db.executeSQL(sql);
                std::cout << "Produit supprimé avec succès.\n";
                break;
            }*/
            case 10: {
                int numeroSerie;
                std::cout << "Entrez le numéro de série du produit à supprimer: ";
                std::cin >> numeroSerie;

                // Supprimer le produit de chaque table spécifique
                ProduitAlimentaire::deleteFromDatabase(db, numeroSerie);
                ProduitElectrique::deleteFromDatabase(db, numeroSerie);
                Jeu::deleteFromDatabase(db, numeroSerie);
                Logiciel::deleteFromDatabase(db, numeroSerie);

                std::cout << "Produit supprimé avec succès de toutes les tables.\n";
                break;
            }

            case 11: {
                long int numSecu;
                std::cout << "Entrez le numéro de sécurité sociale de la personne à supprimer: ";
                std::cin >> numSecu;
                std::string sql = "DELETE FROM PERSONNE WHERE NUMEROSECURITESOCIALE = " + std::to_string(numSecu) + ";";
                db.executeSQL(sql);
                std::cout << "Personne supprimée avec succès.\n";
                break;
            }
            case 12: {
                std::string nomFournisseur;
                std::cout << "Entrez le nom du fournisseur à supprimer: ";
                std::cin >> nomFournisseur;
                std::string sql = "DELETE FROM FOURNISSEUR WHERE NOM = '" + nomFournisseur + "';";
                db.executeSQL(sql);
                std::cout << "Fournisseur supprimé avec succès.\n";
                break;
            }
            case 13: {
                int idAchat;
                std::cout << "Entrez l'ID de l'achat à supprimer: ";
                std::cin >> idAchat;
                std::string sql = "DELETE FROM ACHAT WHERE ID = " + std::to_string(idAchat) + ";";
                db.executeSQL(sql);
                std::cout << "Achat supprimé avec succès.\n";
                break;
            }
            /*case 14: {
                int numeroSerie;
                std::cout << "Entrez le numéro de série du produit à charger: ";
                std::cin >> numeroSerie;
                Produit* produit = Produit::loadFromDatabase(db, numeroSerie);
                if (produit) {
                    //std::cout << "Produit chargé: " << produit->toString() << "\n";
                    delete produit;
                } else {
                    std::cout << "Produit introuvable.\n";
                }
                break;
            }*/
            case 14: {
                int numeroSerie;
                std::cout << "Entrez le numéro de série du produit à charger: ";
                std::cin >> numeroSerie;
                Produit* produit = Produit::loadFromDatabase(db, numeroSerie);
                if (produit) {
                    produits.push_back(produit);  // Ajouter le produit à la liste des produits
                    produit->afficher(); // Afficher le produit chargé
                    std::cout << "Produit chargé avec succès.\n";
                } else {
                    std::cout << "Produit introuvable.\n";
                }
                break;
            }

            case 0:
                std::cout << "Retour au menu principal.\n";
                break;
            default:
                std::cout << "Choix invalide.\n";
                break;
        }
    } while (choixDB != 0);
}


int main() {
    afficherBienvenue();
    int choix;
    do {
        afficherMenu();
        std::cin >> choix;
        switch (choix) {
           case 1: {
                Produit* produit = produit->creerProduit();
                if (produit) {
                    produits.push_back(produit);
                    nbProduits++;
                    std::cout << "Création de produit avec succès.\n";
                } else {
                    std::cout << "Création de produit échouée.\n";
                }
                break;
            }
            case 2: {
                Personne* personne = personne->creerPersonne();
                if (personne && nbPersonnes < MAX_PERSONNES) {
                    personnes[nbPersonnes++] = personne;
                    std::cout << "Création de personnes avec succès.\n";
                } else {
                    std::cout << "Capacité maximale atteinte ou création de personne échouée.\n";
                }
                break;
            }
            case 3: {
                Fournisseur* fournisseur = Fournisseur::creerFournisseur();
                if (fournisseur && nbFournisseurs < MAX_FOURNISSEURS) {
                    fournisseurs[nbFournisseurs++] = fournisseur;
                    std::cout << "Création de fournisseur avec succès.\n";
                } else {
                    std::cout << "Capacité maximale atteinte ou création de fournisseur échouée.\n";
                }
                break;
            }
            case 4: {
                int date;
                std::cout << "Date de l'achat (année): ";
                std::cin >> date;
                Achat* achat = new Achat(date);
                if (nbAchats < MAX_ACHATS) {
                    achats[nbAchats++] = achat;
                    // Ajout de produits et personnes à l'achat
                    int choixAjout;
                    do {
                        std::cout << "1. Ajouter un produit à l'achat\n";
                        std::cout << "2. Ajouter une personne à l'achat\n";
                        std::cout << "0. Terminer l'ajout à l'achat\n";
                        std::cin >> choixAjout;
                        if (choixAjout == 1) {
                            ajouterProduitAchat(achat);
                        } else if (choixAjout == 2) {
                            ajouterPersonneAchat(achat);
                        }
                    } while (choixAjout != 0);
                } else {
                    std::cout << "Capacité maximale des achats atteinte.\n";
                    delete achat;
                }
                break;
            }
            case 5:
                afficherFournisseursProduits();
                break;
            case 6:
                afficherAchatsClient();
                break;
            case 7:
                afficherPersonnelsSousContrat();
                break;
            case 8:
                afficherPersonnelsParSalaire();
                break;
            case 9:
                afficherProduitsParPrix();
                break;
            case 10:
                std::cout << "Valeur du stock: " << calculerValeurStock() << " euros\n";
                break;
            case 11: {
                std::string nomFournisseur;
                std::cout << "Entrez le nom du fournisseur: ";
                std::cin >> nomFournisseur;
                Fournisseur* fournisseur = nullptr;
                for (int i = 0; i < nbFournisseurs; ++i) {
                    if (fournisseurs[i]->getNom() == nomFournisseur) {
                        fournisseur = fournisseurs[i];
                        break;
                    }
                }
                if (fournisseur) {
                    fournisseur->ajouterProduitDepuisSaisie(produits);
                } else {
                    std::cout << "Fournisseur non trouvé.\n";
                }
                break;
            }
            case 12:
                interagirAvecBaseDeDonnees();
                break;
            case 0:
                std::cout << "Au revoir !\n";
                break;
            default:
                std::cout << "Choix invalide.\n";
                break;
        }
    } while (choix != 0);

    // Suppression des objets pour libérer la mémoire
    //for (int i = 0; i < nbProduits; ++i) delete produits[i];
    for (Produit* produit : produits) {
        delete produit;
    }
    //for (int i = 0; i < nbPersonnels; ++i) delete personnels[i];
    for (int i = 0; i < nbPersonnes; ++i) delete personnes[i];
    //for (int i = 0; i < nbClients; ++i) delete clients[i];
    for (int i = 0; i < nbFournisseurs; ++i) delete fournisseurs[i];
    for (int i = 0; i < nbAchats; ++i) delete achats[i];

    return 0;
}