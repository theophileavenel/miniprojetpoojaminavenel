#include <iostream>
#include "Fournisseur.h"
#include "Personne.h"
#include "Produit.h"
#include "Achat.h"
#include "constants.h"
#include "useful.h"

//g++ -g miniprojet2.cpp Fournisseur.cpp Produit.cpp Personne.cpp Achat.cpp useful.cpp -o miniprojet2

//gdb ./miniprojet2

//run

//exit

//Produit* produits[MAX_PRODUITS];
std::vector<Produit*> produits; 
//Personnel* personnels[MAX_PERSONNES];
Personne* personnes[MAX_PERSONNES];
//Client* clients[MAX_PERSONNES];
Fournisseur* fournisseurs[MAX_FOURNISSEURS];
Achat* achats[MAX_ACHATS];

int nbProduits = 0;
int nbPersonnes = 0;
int nbPersonnels = 0;
int nbClients= 0;
int nbFournisseurs = 0;
int nbAchats = 0;

void afficherMenu() {
    std::cout << "\nMenu:\n";
    std::cout << "1. Créer un produit\n";
    std::cout << "2. Créer une personne\n";
    std::cout << "3. Créer un fournisseur\n";
    std::cout << "4. Créer un achat\n";
    std::cout << "5. Afficher les produits d'un fournisseur\n";
    std::cout << "6. Afficher les achats d'un client\n";
    std::cout << "7. Afficher les personnels sous contrat\n";
    std::cout << "8. Afficher les personnels par salaire\n";
    std::cout << "9. Afficher les produits par prix unitaire\n";
    std::cout << "10. Calculer la valeur du stock\n";
    std::cout << "11. Ajouter un produit à un fournisseur\n";

    std::cout << "0. Quitter\n";
}

void afficherFournisseursProduits() {
    std::string nom;
    std::cout << "Nom du fournisseur: ";
    std::cin >> nom;
    for (int i = 0; i < nbFournisseurs; ++i) {
        if (fournisseurs[i]->getNom() == nom) {
            fournisseurs[i]->afficher();
            return;
        }
    }
    std::cout << "Fournisseur non trouvé.\n";
}

void afficherAchatsClient() {
    std::string nom;
    std::cout << "Nom du client: ";
    std::cin >> nom;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getType() == CLIENT && personnes[i]->getNom() == nom) {
            personnes[i]->afficher();
            return;
        }
    }
    std::cout << "Client non trouvé.\n";
}

void afficherPersonnelsSousContrat() {
    int dateActuelle;
    std::cout << "Entrez l'année actuelle: ";
    std::cin >> dateActuelle;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getType() == PERSONNEL && personnes[i]->estSousContrat(dateActuelle)) {
            personnes[i]->afficher();
        }
    }
}

void afficherPersonnelsParSalaire() {
    double salaireMin, salaireMax;
    std::cout << "Salaire minimum: ";
    std::cin >> salaireMin;
    std::cout << "Salaire maximum: ";
    std::cin >> salaireMax;
    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getType() == PERSONNEL ) {
            personnes[i]->afficher();
        }
    }
}


void afficherProduitsParPrix() {
    double prixMin, prixMax;
    std::cout << "Prix minimum: ";
    std::cin >> prixMin;
    std::cout << "Prix maximum: ";
    std::cin >> prixMax;
    for (Produit* produit : produits) {
        if (produit->getPrix() >= prixMin && produit->getPrix() <= prixMax) {
            produit->afficher();
        }
    }
}

double calculerValeurStock() {
    double valeurTotale = 0.0;
    for (Produit* produit : produits) {
        valeurTotale += produit->getPrix() * produit->getQuantite();
    }
    return valeurTotale;
}


void ajouterProduitAchat(Achat* achat) {
    int choixProduit;
    std::cout << "Entrez le numéro de série du produit à ajouter à l'achat: ";
    std::cin >> choixProduit;
    for (Produit* produit : produits) {
        if (produit->getNumeroSerie() == choixProduit) {
            achat->ajouterProduitAchat(produit);
            std::cout << "Produit ajouté à l'achat.\n";
            return;
        }
    }
    std::cout << "Produit non trouvé.\n";
}


void ajouterPersonneAchat(Achat* achat) {
    std::string nomPersonne;
    std::cout << "Entrez le nom de la personne à ajouter à l'achat: ";
    std::cin >> nomPersonne;

    for (int i = 0; i < nbPersonnes; ++i) {
        if (personnes[i]->getNom() == nomPersonne) {
            achat->ajouterPersonneAchat(personnes[i]);
            std::cout << (personnes[i]->getType() == CLIENT ? "Client" : "Personnel") << " ajouté à l'achat.\n";
            return;
        }
    }
    std::cout << "Personne non trouvée.\n";
}


void afficherBienvenue() {
    std::cout << "---------------------------------\n";
    std::cout << "| Bienvenue au Magasin          |\n";
    std::cout << "|                               |\n";
    std::cout << "|   M     M     A     GGGGG     |\n";
    std::cout << "|   MM   MM    A A    G         |\n";
    std::cout << "|   M M M M   A   A   G  GG     |\n";
    std::cout << "|   M  M  M  AAAAAAA  G   G     |\n";
    std::cout << "|   M     M A       A  GGGG     |\n";
    std::cout << "|                               |\n";
    std::cout << "---------------------------------\n";
}

int main() {
    afficherBienvenue();
    int choix;
    do {
        afficherMenu();
        std::cin >> choix;
        switch (choix) {
           case 1: {
                Produit* produit = produit->creerProduit();
                if (produit) {
                    produits.push_back(produit);
                    nbProduits++;
                    std::cout << "Création de produit avec succès.\n";
                } else {
                    std::cout << "Création de produit échouée.\n";
                }
                break;
            }
            case 2: {
                Personne* personne = personne->creerPersonne();
                if (personne && nbPersonnes < MAX_PERSONNES) {
                    personnes[nbPersonnes++] = personne;
                    std::cout << "Création de personnel avec succès.\n";
                } else {
                    std::cout << "Capacité maximale atteinte ou création de personne échouée.\n";
                }
                break;
            }
            case 3: {
                Fournisseur* fournisseur = Fournisseur::creerFournisseur();
                if (fournisseur && nbFournisseurs < MAX_FOURNISSEURS) {
                    fournisseurs[nbFournisseurs++] = fournisseur;
                    std::cout << "Création de fournisseur avec succès.\n";
                } else {
                    std::cout << "Capacité maximale atteinte ou création de fournisseur échouée.\n";
                }
                break;
            }
            case 4: {
                int date;
                std::cout << "Date de l'achat (année): ";
                std::cin >> date;
                Achat* achat = new Achat(date);
                if (nbAchats < MAX_ACHATS) {
                    achats[nbAchats++] = achat;
                    // Ajout de produits et personnes à l'achat
                    int choixAjout;
                    do {
                        std::cout << "1. Ajouter un produit à l'achat\n";
                        std::cout << "2. Ajouter une personne à l'achat\n";
                        std::cout << "0. Terminer l'ajout à l'achat\n";
                        std::cin >> choixAjout;
                        if (choixAjout == 1) {
                            ajouterProduitAchat(achat);
                        } else if (choixAjout == 2) {
                            ajouterPersonneAchat(achat);
                        }
                    } while (choixAjout != 0);
                } else {
                    std::cout << "Capacité maximale des achats atteinte.\n";
                    delete achat;
                }
                break;
            }
            case 5:
                afficherFournisseursProduits();
                break;
            case 6:
                afficherAchatsClient();
                break;
            case 7:
                afficherPersonnelsSousContrat();
                break;
            case 8:
                afficherPersonnelsParSalaire();
                break;
            case 9:
                afficherProduitsParPrix();
                break;
            case 10:
                std::cout << "Valeur du stock: " << calculerValeurStock() << " euros\n";
                break;
            case 11: {
                std::string nomFournisseur;
                std::cout << "Entrez le nom du fournisseur: ";
                std::cin >> nomFournisseur;
                Fournisseur* fournisseur = nullptr;
                for (int i = 0; i < nbFournisseurs; ++i) {
                    if (fournisseurs[i]->getNom() == nomFournisseur) {
                        fournisseur = fournisseurs[i];
                        break;
                    }
                }
                if (fournisseur) {
                    fournisseur->ajouterProduitDepuisSaisie(produits);
                } else {
                    std::cout << "Fournisseur non trouvé.\n";
                }
                break;
            }
            case 0:
                std::cout << "Au revoir !\n";
                break;
            default:
                std::cout << "Choix invalide.\n";
                break;
        }
    } while (choix != 0);

    // Suppression des objets pour libérer la mémoire
    //for (int i = 0; i < nbProduits; ++i) delete produits[i];
    for (Produit* produit : produits) {
        delete produit;
    }
    //for (int i = 0; i < nbPersonnels; ++i) delete personnels[i];
    for (int i = 0; i < nbPersonnes; ++i) delete personnes[i];
    //for (int i = 0; i < nbClients; ++i) delete clients[i];
    for (int i = 0; i < nbFournisseurs; ++i) delete fournisseurs[i];
    for (int i = 0; i < nbAchats; ++i) delete achats[i];

    return 0;
}