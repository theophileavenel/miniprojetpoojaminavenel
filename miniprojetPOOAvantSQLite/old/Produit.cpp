#include <iostream>

// Classe abstraite Produit
class Produit {
protected:
    int numeroserie;
    int quantite;
    double prixunitaire;

public:
    Produit(int numeroserie, int quantite, double prixunitaire)
        : numeroserie(numeroserie), quantite(quantite), prixunitaire(prixunitaire) {}

    virtual void afficher() const = 0; // Méthode pure virtuelle pour définir la classe abstraite

    virtual ~Produit() {
        //std::cout << "Le destructeur va supprimer l'objet de type Produit." << std::endl;
    }

};

// Produits alimentaires
class ProduitAlimentaire : public Produit {
private:
    int datePeremption; //Année de péremption

public:
    ProduitAlimentaire(int numeroserie, int quantite, double prixunitaire, const int datePeremption)
        : Produit(numeroserie, quantite, prixunitaire), datePeremption(datePeremption) {}

    ~ProduitAlimentaire() { std::cout << "Le destructeur va supprimer l'objet de type ProduitAlimentaire." << std::endl;}


    void afficher() const  {
        std::cout << "Produit Alimentaire - Numéro de série: " << numeroserie 
                  << ", Quantité: " << quantite 
                  << ", Prix unitaire: " << prixunitaire 
                  << ", Date de péremption: " << datePeremption << std::endl;
    }
};

// Produits éléctriques
class ProduitElectrique : public Produit {
private:
    double puissance;
    int dureeGarantie; //Durée de la garantie s'exprime en année

public:
    ProduitElectrique(int numeroserie, int quantite, double prixunitaire, double puissance, int dureeGarantie)
        : Produit(numeroserie, quantite, prixunitaire), puissance(puissance), dureeGarantie(dureeGarantie) {}

    ~ProduitElectrique() { std::cout << "Le destructeur va supprimer l'objet de type ProduitElectrique." << std::endl;}

    void afficher() const  {
        std::cout << "Produit Electrique - Numéro de série: " << numeroserie 
                  << ", Quantité: " << quantite 
                  << ", Prix unitaire: " << prixunitaire 
                  << ", Puissance: " << puissance 
                  << "W, Durée de garantie: " << dureeGarantie << " ans" << std::endl;
    }
};

// Jeux
class Jeu : public Produit {
private:
    int ageMinimum;

public:
    Jeu(int numeroserie, int quantite, double prixunitaire, int ageMinimum)
        : Produit(numeroserie, quantite, prixunitaire), ageMinimum(ageMinimum) {}

    ~Jeu() { std::cout << "Le destructeur va supprimer l'objet de type Jeu." << std::endl;}

    void afficher() const  {
        std::cout << "Jeu - Numéro de série: " << numeroserie 
                  << ", Quantité: " << quantite 
                  << ", Prix unitaire: " << prixunitaire 
                  << ", Age minimum: " << ageMinimum << " ans" << std::endl;
    }
};

// Logiciels
class Logiciel : public Produit {
private:
    std::string systemeExploitation;
    std::string caracteristiquesNecessaires;
    std::string categorie;

public:
    Logiciel(int numeroserie, int quantite, double prixunitaire, const std::string& systemeExploitation, const std::string& caracteristiquesNecessaires, const std::string& categorie)
        : Produit(numeroserie, quantite, prixunitaire), systemeExploitation(systemeExploitation), caracteristiquesNecessaires(caracteristiquesNecessaires), categorie(categorie) {}

    ~Logiciel() { std::cout << "Le destructeur va supprimer l'objet de type Logiciel." << std::endl;}
    
    void afficher() const {
        std::cout << "Logiciel - Numéro de série: " << numeroserie 
                  << ", Quantité: " << quantite 
                  << ", Prix unitaire: " << prixunitaire 
                  << ", Système d'exploitation: " << systemeExploitation 
                  << ", Caractéristiques nécessaires: " << caracteristiquesNecessaires 
                  << ", Catégorie: " << categorie << std::endl;
    }
};


int main() {
    // Création des objets Produit
    Produit* p1 = new ProduitAlimentaire(101, 50, 3.99, 2024);
    Produit* p2 = new ProduitElectrique(102, 20, 99.99, 1500, 2);
    Produit* p3 = new Jeu(103, 30, 49.99, 12);
    Produit* p4 = new Logiciel(104, 10, 199.99, "Windows 10", "8GB RAM, 500GB Disk", "Bureautique");

    // Affichage des produits
    p1->afficher();
    p2->afficher();
    p3->afficher();
    p4->afficher();

    // Suppression des objets pour libérer la mémoire
    delete p1;
    delete p2;
    delete p3;
    delete p4;

    return 0;
}
