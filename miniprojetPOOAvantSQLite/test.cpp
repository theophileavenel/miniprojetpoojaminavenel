#include <iostream>
#include <string>

// Fonction pour vérifier si une chaîne contient des espaces
bool contientEspaces(const std::string& str) {
    for (char ch : str) {
        if (ch == ' ') {
            return true;
        }
    }
    return false;
}

int main() {
    std::string nom;

    // Saisir le nom du fournisseur et vérifier l'absence d'espaces
    do {
        std::cout << "Nom du fournisseur (sans espaces): ";
        std::cin >> nom;
        if (contientEspaces(nom)) {
            std::cout << "Erreur: Le nom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
        }
    } while (contientEspaces(nom));

    std::cout << "Nom du fournisseur accepté: " << nom << std::endl;

    return 0;
}
