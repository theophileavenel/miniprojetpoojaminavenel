#include "Achat.h"

// Initialisation de la variable statique
int Achat::dernierNumeroAchat = 0;

// Constructeur de la classe Achat
Achat::Achat(int date) : dateAchat(date), nbProduits(0), nbPersonnes(0) {
    numeroAchat = ++dernierNumeroAchat; // Incrémente et attribue le numéro d'achat
}

Achat::~Achat() {
    std::cout << "Le destructeur va supprimer l'objet de type Achat." << std::endl;
}

// Méthode pour ajouter un produit dans le tableau des produits achetés
void Achat::ajouterProduitAchat(Produit* produit) {
    if (nbProduits < MAX_PRODUITS) {
        produits[nbProduits++] = produit;
    } else {
        std::cout << "Impossible d'ajouter plus de produits, limite atteinte." << std::endl;
    }
}

// Méthode pour ajouter une personne dans le tableau des personnes concernées
void Achat::ajouterPersonneAchat(Personne* personne) {
    if (nbPersonnes < MAX_PERSONNES) {
        personnes[nbPersonnes++] = personne;
    } else {
        std::cout << "Impossible d'ajouter plus de personnes, limite atteinte." << std::endl;
    }
}

// Méthode pour afficher l'achat réalisé
void Achat::afficherDetails() const {
    std::cout << "Date de l'achat : " << dateAchat << std::endl;
    std::cout << "Produits achetés : " << std::endl;
    for (int i = 0; i < nbProduits; ++i) {
        produits[i]->afficher();
    }
    std::cout << "Personnes concernées : " << std::endl;
    for (int i = 0; i < nbPersonnes; ++i) {
        personnes[i]->afficher();
    }
}

int Achat::getNumeroAchat() const {
    return numeroAchat;
}
