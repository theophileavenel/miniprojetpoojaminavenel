#ifndef ACHAT_H
#define ACHAT_H

#include <iostream>
#include <string>
#include "Personne.h"
#include "Produit.h"
#include "constants.h"
#include "useful.h"

class Achat {
private:
    static int dernierNumeroAchat; // Variable statique pour suivre le dernier numéro d'achat
    int dateAchat; //année achat
    int numeroAchat;
    Produit* produits[MAX_PRODUITS];
    Personne* personnes[MAX_PERSONNES];
    int nbProduits;
    int nbPersonnes;

public:
    Achat(int date);
    ~Achat();
    void ajouterProduitAchat(Produit* produit);
    void ajouterPersonneAchat(Personne* personne);
    void afficherDetails() const;
    int getNumeroAchat() const;
};

#endif // ACHAT_H
