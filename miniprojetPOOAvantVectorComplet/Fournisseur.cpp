#include "Fournisseur.h"

Fournisseur::Fournisseur(const std::string& n, const std::string& coord, int codePays, int numeroTel)
    : nom(n), coordonnees(coord), codePays(codePays), numeroTel(numeroTel), nbProduits(0) {}

Fournisseur::~Fournisseur() {
    std::cout << "Le destructeur va supprimer l'objet de type Fournisseur." << std::endl;
}

std::string Fournisseur::getNom() const {
    return nom;
}

std::string Fournisseur::getCoordonnees() const{
    return coordonnees;
}

int Fournisseur::getCodePays() const{
    return codePays;
}

int Fournisseur::getNumeroTel() const{
    return numeroTel;
}

int Fournisseur::getNbProduits() const{
    return nbProduits;
}

/*
Produit* Fournisseur::getProduitsFournisseur() const{
    return produits;
}*/

std::vector<Produit*> Fournisseur::getProduitsFournisseur() const {
    return produits;
}

/*void Fournisseur::ajouterProduit(Produit* produit) {
    if (nbProduits < MAX_PRODUITS) {
        produits[nbProduits++] = produit;
    } else {
        std::cout << "Impossible d'ajouter plus de produits." << std::endl;
    }
}*/

void Fournisseur::ajouterProduit(Produit* produit) {
    produits.push_back(produit);
}


void Fournisseur::ajouterProduitDepuisSaisie(std::vector<Produit*>& tousLesProduits) {
    int numeroSerie;
    std::cout << "Quel produit voulez-vous rentrer (numéro de série) ? ";
    std::cin >> numeroSerie;

    Produit* produit = trouverProduitParNumeroSerie(tousLesProduits, numeroSerie);
    if (produit != nullptr) {
        ajouterProduit(produit);
        std::cout << "Produit ajouté au fournisseur : " << produit->getNumeroSerie() << std::endl;
    } else {
        std::cout << "Produit avec le numéro de série " << numeroSerie << " non trouvé." << std::endl;
    }
}

Produit* Fournisseur::trouverProduitParNumeroSerie(const std::vector<Produit*>& tousLesProduits, int numeroSerie) {
    for (Produit* produit : tousLesProduits) {
        if (produit->getNumeroSerie() == numeroSerie) {
            return produit;
        }
    }
    return nullptr;
}

void Fournisseur::afficher() const {
    std::cout << "Nom du fournisseur : " << nom << std::endl;
    std::cout << "Coordonnées : " << coordonnees << std::endl;
    std::cout << "Téléphone : +" << codePays << " " << numeroTel << std::endl;
    std::cout << "Produits fournis :" << std::endl;
    for (int i = 0; i < nbProduits; ++i) {
        produits[i]->afficher(); // Utiliser la méthode afficher() de la classe Produit
    }
}

Fournisseur* Fournisseur::creerFournisseur() {
    std::string nom, adresse;
    int codePays, numeroTel;

    // Saisir le nom du fournisseur et vérifier l'absence d'espaces
    do {
        std::cout << "Nom du fournisseur (sans espaces): ";
        std::getline(std::cin, nom);
        if (contientEspaces(nom) || nom.empty()) {
            std::cout << "Erreur: Le nom ne doit pas contenir d'espaces et ne doit pas être vide. Veuillez réessayer.\n";
        }
    } while (contientEspaces(nom) || nom.empty());
    //std::cout << "Nom du fournisseur retenu: " << nom << std::endl;

    // Saisir l'adresse du fournisseur et vérifier l'absence d'espaces
    do {
        std::cout << "Adresse (sans espaces): ";
        std::getline(std::cin, adresse);
        if (contientEspaces(adresse)) {
            std::cout << "Erreur: L'adresse ne doit pas contenir d'espaces. Veuillez réessayer.\n";
        }
    } while (contientEspaces(adresse));

    std::cout << "Code pays: ";
    // Petit test
    while (!(std::cin >> codePays) || codePays < 1 || codePays > 999) {
        std::cout << "Entrée invalide. Veuillez entrer un code pays valide (1-999): ";

    }

    std::cout << "Numéro de téléphone (10 chiffres): ";
    // Petit test
    while (!(std::cin >> numeroTel) || numeroTel < 1000000000 || numeroTel > 9999999999) {
        std::cout << "Entrée invalide. Veuillez entrer un numéro de téléphone valide (10 chiffres): ";
    }
    std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée

    return new Fournisseur(nom, adresse, codePays, numeroTel);
}