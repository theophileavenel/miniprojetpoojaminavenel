#ifndef FOURNISSEUR_H
#define FOURNISSEUR_H

#include <iostream>
#include <string>
#include <vector>
#include "Produit.h"
#include "constants.h"
#include "useful.h"

class Fournisseur {
private:
    std::string nom;
    std::string coordonnees;
    int codePays;
    int numeroTel;
    std::vector<Produit*> produits; // Utilisation de vector au lieu de tableau C-style
    int nbProduits;

public:
    Fournisseur(const std::string& nom, const std::string& coord, int codePays, int numeroTel);
    ~Fournisseur();

    void ajouterProduitDepuisSaisie(std::vector<Produit*>& tousLesProduits);  // Méthode pour ajouter un produit

    void ajouterProduit(Produit* produit);
    Produit* trouverProduitParNumeroSerie(const std::vector<Produit*>& tousLesProduits, int numeroSerie);
    void afficher() const;
    static Fournisseur* creerFournisseur();

    std::string getNom() const;
    std::string getCoordonnees() const;
    int getCodePays() const;
    int getNumeroTel() const;
    int getNbProduits() const;
    std::vector<Produit*> getProduitsFournisseur() const; // Changement de type de retour
};

#endif // FOURNISSEUR_H
