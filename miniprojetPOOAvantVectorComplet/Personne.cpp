#include "Personne.h"
#include "Achat.h"

// Implémentation de la classe Personne
Personne::Personne(int datenaissance, long int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
    : datenaissance(datenaissance), numerosecuritesociale(numerosecuritesociale), adressemail(adressemail), nom(nom), prenom(prenom) {}

Personne::~Personne() {
    //std::cout << "Le destructeur va supprimer l'objet de type Personne." << std::endl;
}

std::string Personne::getNom() const {
    return nom;
}

std::string Personne::getPrenom() const{
    return prenom;
}

std::string Personne::getAdresseMail() const{
    return adressemail;
}


long int Personne::getNumerosecuritesociale() const{
    return numerosecuritesociale;
}

int Personne::getDateNaissance() const{
    return datenaissance;
}

Personne* Personne::creerPersonne() {
    int choix;
    std::cout << "Choisissez le type de personne:\n";
    std::cout << "1. Personnel\n";
    std::cout << "2. Client\n";
    std::cin >> choix;

    int datenaissance, datedebut, datefin;
    long int numerosecuritesociale;
    std::string adressemail, nom, prenom, role;
    double salaire;

    switch (choix) {
        case 1:
            //std::cout << "Date de naissance: "; std::cin >> datenaissance;
            std::cout << "Date de naissance: (année) comprise entre 1850 et 2024 ";
            while (!(std::cin >> datenaissance) || datenaissance < 1850 || datenaissance > 2024) {
                std::cout << "Entrée invalide. Veuillez entrer une année de naissance valide (1850<datenaissance<2024): ";
            }
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée

            //Numéro sociale est composé de 13 chiffres
            //il faut donc un bigint ou long int
            //std::cout << "Numéro de sécurité sociale: "; std::cin >> numerosecuritesociale;
            std::cout << "Numéro de sécurité sociale de 13 chiffres ";
            while (!(std::cin >> numerosecuritesociale) || numerosecuritesociale > 9999999999999 ) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de sécurité sociale valide et composé de 13 chiffres: ";
            }
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée
            //std::cout << "Adresse email: "; std::cin >> adressemail;
            do {
                std::cout << "Adresse email: du Personnel (sans espaces): ";
                std::getline(std::cin, nom);
                if (contientEspaces(adressemail)) {
                    std::cout << "Erreur: Le nom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(adressemail));
            //std::cout << "Nom: "; std::cin >> nom;
             // Saisir le nom du fournisseur et vérifier l'absence d'espaces
            do {
                std::cout << "Nom du Personnel (sans espaces): ";
                std::getline(std::cin, nom);
                if (contientEspaces(nom)) {
                    std::cout << "Erreur: Le nom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(nom));
            //std::cout << "Prénom: "; std::cin >> prenom;
            do {
                std::cout << "Prénom du Personnel (sans espaces): ";
                std::getline(std::cin, prenom);
                if (contientEspaces(prenom)) {
                    std::cout << "Erreur: Le prénom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(prenom));
            std::cout << "Salaire: "; std::cin >> salaire;

            //std::cout << "Date de début de travail: "; std::cin >> datedebut;
            std::cout << "Date de début de travail: (année) comprise entre 1850 et 2030 ";
            while (!(std::cin >> datedebut) || datedebut < 1850 || datedebut > 2030) {
                std::cout << "Entrée invalide. Veuillez entrer une date de début de travail valide (1850<datenaissance<2030): ";
            }
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée

            std::cout << "Date de fin de travail: "; std::cin >> datefin;
            std::cout << "Date de fin de travail: (année) comprise entre 1850 et 2030 ";
            while (!(std::cin >> datefin) || datefin < 1850 || datefin > 2030) {
                std::cout << "Entrée invalide. Veuillez entrer une date de fin de travail valide (1850<datenaissance<2030): ";
            }
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée

            //std::cout << "Rôle: "; std::cin >> role;
            do {
                std::cout << "Rôle du personnel (sans espaces): ";
                std::getline(std::cin, role);
                if (contientEspaces(role)) {
                    std::cout << "Erreur: Le role ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(role));
            return new Personnel(datenaissance, numerosecuritesociale, adressemail, nom, prenom, salaire, datedebut, datefin, role);
        case 2:
            //std::cout << "Date de naissance: "; std::cin >> datenaissance;
            std::cout << "Date de naissance: (année) comprise entre 1850 et 2024 ";
            while (!(std::cin >> datenaissance) || datenaissance < 1850 || datenaissance > 2024) {
                std::cout << "Entrée invalide. Veuillez entrer une année de naissance valide (1850<datenaissance<2024): ";
            }
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée

            //Numéro sociale est composé de 13 chiffres
            //il faut donc un bigint ou long int
            //std::cout << "Numéro de sécurité sociale: "; std::cin >> numerosecuritesociale;
            std::cout << "Numéro de sécurité sociale de 13 chiffres ";
            while (!(std::cin >> numerosecuritesociale) || numerosecuritesociale > 9999999999999 ) {
                std::cout << "Entrée invalide. Veuillez entrer un numéro de sécurité sociale valide et composé de 13 chiffres: ";
            }
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée
            //std::cout << "Adresse email: "; std::cin >> adressemail;
            do {
                std::cout << "Adresse email: du client (sans espaces): ";
                std::cin >> nom;
                if (contientEspaces(nom)) {
                    std::cout << "Erreur: Le nom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(nom));
            //std::cout << "Nom: "; std::cin >> nom;
             // Saisir le nom du fournisseur et vérifier l'absence d'espaces
            do {
                std::cout << "Nom du client (sans espaces): ";
                std::cin >> nom;
                if (contientEspaces(nom)) {
                    std::cout << "Erreur: Le nom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(nom));
            //std::cout << "Prénom: "; std::cin >> prenom;
            do {
                std::cout << "Prénom du client (sans espaces): ";
                std::cin >> prenom;
                if (contientEspaces(prenom)) {
                    std::cout << "Erreur: Le prénom ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(prenom));
            return new Client(datenaissance, numerosecuritesociale, adressemail, nom, prenom);
        default:
            std::cout << "Choix invalide.\n";
            return nullptr;
    }
}

// Implémentation de la classe Personnel
/*Personnel::Personnel(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
                     double salaire, int datedebut, int datefin, const std::string& role)
    : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), salaire(salaire), datedebut(datedebut), datefin(datefin), role(role) {}*/

Personnel::~Personnel() {
    std::cout << "Le destructeur va supprimer l'objet de type Personnel." << std::endl;
}

void Personnel::afficher() const {
    std::cout << "Bonjour, je suis " << prenom << " " << nom
              << " et je suis né en " << datenaissance // Sa date de naissance (on va utiliser l’année seulement pour ce projet)
              << ". Mon adresse mail est : " << adressemail 
              << ". Je suis membre du personnel depuis l'année " << datedebut 
              << " et ma date de fin de travail est " << datefin 
              << ". Mon rôle est " << role 
              << " et mon salaire est de " << salaire
              << std::endl;
}

double Personnel::getSalaire() const {
    return salaire;
}

int Personnel::getDateDebut() const {
    return datedebut;
}

int Personnel::getDateFin() const {
    return datefin;
}

std::string Personnel::getRole() const {
    return role;
}

bool Personnel::estSousContrat(int dateActuelle) const {
    return (dateActuelle >= datedebut && dateActuelle <= datefin);
}

// Constructeur de la classe Client
/*Client::Client(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
    : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), nombreAchats(0) {}*/

Client::~Client() {
    std::cout << "Le destructeur va supprimer l'objet de type Client." << std::endl;
    for (int i = 0; i < nombreAchats; ++i) {
        delete anciensAchats[i];
    }
}

// Méthode pour ajouter un achat dans le tableau des anciens achats
void Client::ajouterAchat(Achat* achat) {
    if (nombreAchats < MAX_ACHATS) {
        anciensAchats[nombreAchats++] = achat;
    } else {
        std::cout << "Liste d'achats pleine, impossible d'ajouter plus d'achats." << std::endl;
    }
}

// Méthode pour afficher les achats du client
void Client::afficherAchats() const {
    std::cout << "Achats de " << nom << " :" << std::endl;
    for (int i = 0; i < nombreAchats; ++i) {
        anciensAchats[i]->afficherDetails();
    }
}

// Méthode pour afficher les informations du client
void Client::afficher() const {
    std::cout << "Bonjour, je suis " << prenom << " " << nom
              << " et je suis né en " << datenaissance
              << ". Mon adresse mail est : " << adressemail;

    if (nombreAchats > 0) {
        std::cout << ". Voici le numéro de mon dernier achat : " 
                  << anciensAchats[nombreAchats - 1]->getNumeroAchat() << std::endl;
    } else {
        std::cout << ". Je n'ai pas encore fait d'achats." << std::endl;
    }
}