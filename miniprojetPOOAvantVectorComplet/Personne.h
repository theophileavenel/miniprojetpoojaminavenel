#ifndef PERSONNE_H
#define PERSONNE_H

#include <iostream>
#include <string>
#include "constants.h"
#include "useful.h"

//#include "Achat.h"
class Achat;

// Classe abstraite Personne
class Personne {
protected:
    int datenaissance; // Sa date de naissance (on va utiliser l’année seulement pour ce projet)
    //bigint car 13 chiffres
    long int numerosecuritesociale;
    std::string adressemail;
    std::string nom;
    std::string prenom;
    TypePersonne type;
public:
    Personne(int datenaissance, long int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom);

    virtual void afficher() const = 0; // Méthode pure virtuelle pour définir la classe abstraite

    TypePersonne getType() const { return type; }

    virtual ~Personne();

    static Personne* creerPersonne();

    virtual std::string getNom() const;

    virtual std::string getPrenom() const;
    virtual std::string getAdresseMail() const;
    virtual long int getNumerosecuritesociale() const;
    virtual int getDateNaissance() const;

    virtual double getSalaire() const { return 0.0; }  // Méthode virtuelle pure pour les personnels
    virtual bool estSousContrat(int dateActuelle) const { return false; }  // Méthode virtuelle pure pour les personnels
};

// Classe Personnel
class Personnel : public Personne {
private:
    double salaire;
    int datedebut;
    int datefin;
    std::string role;

public:
    /*Personnel(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
              double salaire, int datedebut, int datefin, const std::string& role);*/
    Personnel(int datenaissance, long int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
              double salaire, int datedebut, int datefin, const std::string& role)
        : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), salaire(salaire), datedebut(datedebut), datefin(datefin), role(role) {
        type = PERSONNEL;
    }

    ~Personnel();

    void afficher() const;

    double getSalaire() const;
    int getDateDebut() const;
    int getDateFin() const;
    std::string getRole() const;

    bool estSousContrat(int dateActuelle) const;
};

// Classe Client
class Client : public Personne {
private:
    Achat* anciensAchats[MAX_ACHATS];
    int nombreAchats;

public:
    /*Client(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom);*/
    Client(int datenaissance, long int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
        : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom) {
        type = CLIENT;
    }
    ~Client();
    void ajouterAchat(Achat* achat);
    void afficherAchats() const;
    void afficher() const;
};

#endif // PERSONNE_H
