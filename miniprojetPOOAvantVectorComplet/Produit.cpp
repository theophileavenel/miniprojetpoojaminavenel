#include "Produit.h"

// Implémentation de la classe Produit
Produit::Produit(int numeroserie, int quantite, double prixunitaire)
    : numeroserie(numeroserie), quantite(quantite), prixunitaire(prixunitaire) {}

Produit::~Produit() {
    //std::cout << "Le destructeur va supprimer l'objet de type Produit." << std::endl;
}

double Produit::getPrix() const {
    return prixunitaire;
}

int Produit::getQuantite() const {
    return quantite;
}

int Produit::getNumeroSerie() const {
    return numeroserie;
}

// Implémentation de la classe ProduitAlimentaire
ProduitAlimentaire::ProduitAlimentaire(int numeroserie, int quantite, double prixunitaire, const int datePeremption)
    : Produit(numeroserie, quantite, prixunitaire), datePeremption(datePeremption) {}

ProduitAlimentaire::~ProduitAlimentaire() {
    std::cout << "Le destructeur va supprimer l'objet de type ProduitAlimentaire." << std::endl;
}

void ProduitAlimentaire::afficher() const {
    std::cout << "Produit Alimentaire - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Date de péremption: " << datePeremption << std::endl;
}

int ProduitAlimentaire::getDatePeremption() const{
    return datePeremption;
}

// Implémentation de la classe ProduitElectrique
ProduitElectrique::ProduitElectrique(int numeroserie, int quantite, double prixunitaire, double puissance, int dureeGarantie)
    : Produit(numeroserie, quantite, prixunitaire), puissance(puissance), dureeGarantie(dureeGarantie) {}

ProduitElectrique::~ProduitElectrique() {
    std::cout << "Le destructeur va supprimer l'objet de type ProduitElectrique." << std::endl;
}

void ProduitElectrique::afficher() const {
    std::cout << "Produit Electrique - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Puissance: " << puissance 
              << "W, Durée de garantie: " << dureeGarantie << " ans" << std::endl;
}

// Implémentation de la classe Jeu
Jeu::Jeu(int numeroserie, int quantite, double prixunitaire, int ageMinimum)
    : Produit(numeroserie, quantite, prixunitaire), ageMinimum(ageMinimum) {}

Jeu::~Jeu() {
    std::cout << "Le destructeur va supprimer l'objet de type Jeu." << std::endl;
}

void Jeu::afficher() const {
    std::cout << "Jeu - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Age minimum: " << ageMinimum << " ans" << std::endl;
}

int Jeu::getAgeMinimum() const{
    return ageMinimum;
}

// Implémentation de la classe Logiciel
Logiciel::Logiciel(int numeroserie, int quantite, double prixunitaire, const std::string& systemeExploitation, const std::string& caracteristiquesNecessaires, const std::string& categorie)
    : Produit(numeroserie, quantite, prixunitaire), systemeExploitation(systemeExploitation), caracteristiquesNecessaires(caracteristiquesNecessaires), categorie(categorie) {}

Logiciel::~Logiciel() {
    std::cout << "Le destructeur va supprimer l'objet de type Logiciel." << std::endl;
}

void Logiciel::afficher() const {
    std::cout << "Logiciel - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Système d'exploitation: " << systemeExploitation 
              << ", Caractéristiques nécessaires: " << caracteristiquesNecessaires 
              << ", Catégorie: " << categorie << std::endl;
}

std::string Logiciel::getSystemeExploitation() const {
    return systemeExploitation;
}

std::string Logiciel::getCaracteristiquesNecessaires() const{
    return caracteristiquesNecessaires;
}

std::string Logiciel::getCategorie() const{
    return categorie;
}

Produit* Produit::creerProduit() {
    int choix;
    std::cout << "Choisissez le type de produit:\n";
    std::cout << "1. Produit Alimentaire\n";
    std::cout << "2. Produit Electrique\n";
    std::cout << "3. Jeu\n";
    std::cout << "4. Logiciel\n";
    std::cin >> choix;

    int numero, quantite, datePeremption, puissance, dureeGarantie, ageMinimum;
    double prix;
    std::string systemeExploitation, caracteristiquesNecessaires, categorie;

    switch (choix) {
        case 1:
            std::cout << "Numéro de série: "; std::cin >> numero;
            std::cout << "Quantité: "; std::cin >> quantite;
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée

            std::cout << "Prix unitaire: "; std::cin >> prix;
            std::cout << "Date de péremption: "; std::cin >> datePeremption;
            return new ProduitAlimentaire(numero, quantite, prix, datePeremption);
        case 2:
            std::cout << "Numéro de série: "; std::cin >> numero;
            std::cout << "Quantité: "; std::cin >> quantite;
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée            std::cout << "Prix unitaire: "; std::cin >> prix;
            std::cout << "Puissance: "; std::cin >> puissance;
            std::cout << "Durée de garantie: "; std::cin >> dureeGarantie;
            return new ProduitElectrique(numero, quantite, prix, puissance, dureeGarantie);
        case 3:
            std::cout << "Numéro de série: "; std::cin >> numero;
            std::cout << "Quantité: "; std::cin >> quantite;
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée            std::cout << "Prix unitaire: "; std::cin >> prix;
            std::cout << "Age minimum: "; std::cin >> ageMinimum;
            return new Jeu(numero, quantite, prix, ageMinimum);
        case 4:
            std::cout << "Numéro de série: "; std::cin >> numero;
            std::cout << "Quantité: "; std::cin >> quantite;
            std::cout << std::endl;  // Ajouter un saut de ligne après l'entrée            std::cout << "Prix unitaire: "; std::cin >> prix;
            //std::cout << "Système d'exploitation: "; std::cin >> systemeExploitation;
            do {
                std::cout << "Système d'exploitation (sans espaces): ";
                std::getline(std::cin, systemeExploitation);

                if (contientEspaces(systemeExploitation)) {
                    std::cout << "Erreur: Le système d'exploitation ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(systemeExploitation));

            //std::cout << "Caractéristiques nécessaires:  "; std::cin >> caracteristiquesNecessaires;
            do {
                std::cout << "Caractéristiques nécessaires:(sans espaces): ";
                std::getline(std::cin, caracteristiquesNecessaires);

                if (contientEspaces(caracteristiquesNecessaires)) {
                    std::cout << "Erreur: La Caractéristiques nécessaires: ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(caracteristiquesNecessaires));
            //std::cout << "Catégorie: "; std::cin >> categorie;
            do {
                std::cout << "categorie:(sans espaces): ";
                std::getline(std::cin, categorie);

                if (contientEspaces(categorie)) {
                    std::cout << "Erreur: La categorie ne doit pas contenir d'espaces. Veuillez réessayer.\n";
                }
            } while (contientEspaces(categorie));

            return new Logiciel(numero, quantite, prix, systemeExploitation, caracteristiquesNecessaires, categorie);
        default:
            std::cout << "Choix invalide.\n";
            return nullptr;
    }
}
