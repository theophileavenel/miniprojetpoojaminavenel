#ifndef PRODUIT_H
#define PRODUIT_H

#include <iostream>
#include <string>
#include "useful.h"


// Classe abstraite Produit
class Produit {
protected:
    int numeroserie;
    int quantite;
    double prixunitaire;

public:
    Produit(int numeroserie, int quantite, double prixunitaire);

    virtual void afficher() const = 0; // Méthode pure virtuelle pour définir la classe abstraite

    static Produit* creerProduit();

    virtual ~Produit();

    double getPrix() const;
    int getQuantite() const;
    int getNumeroSerie() const; // Déclaration de la méthode getNumeroSerie
};

// Produits alimentaires
class ProduitAlimentaire : public Produit {
private:
    int datePeremption; // Année de péremption

public:
    ProduitAlimentaire(int numeroserie, int quantite, double prixunitaire, const int datePeremption);
    
    ~ProduitAlimentaire();

    int getDatePeremption() const;

    void afficher() const ;
};

// Produits éléctriques
class ProduitElectrique : public Produit {
private:
    double puissance;
    int dureeGarantie; // Durée de la garantie s'exprime en année

public:
    ProduitElectrique(int numeroserie, int quantite, double prixunitaire, double puissance, int dureeGarantie);
    
    ~ProduitElectrique();

    double getPuissance() const;

    int getDureeGarantie() const;

    void afficher() const;
};

// Jeux
class Jeu : public Produit {
private:
    int ageMinimum;

public:
    Jeu(int numeroserie, int quantite, double prixunitaire, int ageMinimum);
    
    ~Jeu();

    int getAgeMinimum() const;

    void afficher() const ;
};

// Logiciels
class Logiciel : public Produit {
private:
    std::string systemeExploitation;
    std::string caracteristiquesNecessaires;
    std::string categorie;

public:
    Logiciel(int numeroserie, int quantite, double prixunitaire, const std::string& systemeExploitation, const std::string& caracteristiquesNecessaires, const std::string& categorie);
    
    ~Logiciel();

    void afficher() const ;
    std::string getSystemeExploitation() const;

    std::string getCaracteristiquesNecessaires() const;

    std::string getCategorie() const;

};

#endif // PRODUIT_H
