#include "useful.h"

bool contientEspaces(const std::string& str) {
    for (char ch : str) {
        if (ch == ' ') {
            return true;
        }
    }
    return false;
}
