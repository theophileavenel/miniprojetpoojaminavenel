#ifndef USEFUL_H
#define USEFUL_H

#include <iostream>
#include <string>

enum TypePersonne {
    CLIENT,
    PERSONNEL
};

bool contientEspaces(const std::string& str);

#endif // USEFUL_H
