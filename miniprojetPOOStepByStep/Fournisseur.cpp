#include "Fournisseur.h"

Fournisseur::Fournisseur(const std::string& nom) : nom(nom), nbProduits(0) {}

Fournisseur::~Fournisseur() {
    std::cout << "Le destructeur va supprimer l'objet de type Fournisseur." << std::endl;
}

void Fournisseur::ajouterProduit(Produit* produit) {
    if (nbProduits < MAX_PRODUITS) {
        produits[nbProduits++] = produit;
    } else {
        std::cerr << "Impossible d'ajouter plus de produits, limite atteinte." << std::endl;
    }
}

void Fournisseur::afficher() const {
    std::cout << "Fournisseur: " << nom << std::endl;
    std::cout << "Produits fournis : " << std::endl;
    for (int i = 0; i < nbProduits; ++i) {
        produits[i]->afficher();
    }
}
