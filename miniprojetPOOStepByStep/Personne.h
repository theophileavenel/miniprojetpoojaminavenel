#ifndef PERSONNE_H
#define PERSONNE_H

#include <iostream>
#include <string>
#include "constants.h"

// Déclaration avancée de la classe Achat
class Achat;

// Classe abstraite Personne
class Personne {
protected:
    int datenaissance; // Sa date de naissance (on va utiliser l’année seulement pour ce projet)
    int numerosecuritesociale;
    std::string adressemail;
    std::string nom;
    std::string prenom;
public:
    Personne(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom);

    virtual void afficher() const = 0; // Méthode pure virtuelle pour définir la classe abstraite

    virtual ~Personne();
};

// Classe Personnel
class Personnel : public Personne {
private:
    double salaire;
    int datedebut;
    int datefin;
    std::string role;

public:
    Personnel(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
              double salaire, int datedebut, int datefin, const std::string& role);

    ~Personnel();

    void afficher() const ;
};

// Classe Client
class Client : public Personne {
public:
    Client(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom);
    ~Client();
    
    void afficher() const;
    void ajouterAchat(Achat* achat);
    void afficherAchats() const;

private:
    std::string prenom;
    std::string nom;
    std::string datenaissance;
    std::string adressemail;
    Achat* anciensAchats[MAX_ACHATS];
    int nombreAchats;
};

#endif // CLIENT_H

