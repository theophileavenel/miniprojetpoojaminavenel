#include "Achat.h"
#include "constants.h"
#include <iostream>

// Constructeur
Achat::Achat(int date) : date_achat(date), nombreProduits(0), nombrePersonnes(0) {}

// Destructeur
Achat::~Achat() {
    for (int i = 0; i < nombreProduits; ++i) {
        delete produits[i];
    }
    for (int i = 0; i < nombrePersonnes; ++i) {
        delete personnes[i];
    }
}

// Ajouter un produit à l'achat
void Achat::ajouterProduitAchat(Produit* produit) {
    if (nombreProduits < MAX_PRODUITS) {
        produits[nombreProduits++] = produit;
    } else {
        std::cerr << "Erreur : nombre maximum de produits atteint." << std::endl;
    }
}

// Ajouter une personne à l'achat
void Achat::ajouterPersonneAchat(Personne* personne) {
    if (nombrePersonnes < MAX_PERSONNES) {
        personnes[nombrePersonnes++] = personne;
    } else {
        std::cerr << "Erreur : nombre maximum de personnes atteint." << std::endl;
    }
}

// Afficher les détails de l'achat
void Achat::afficher() const {
    std::cout << "Date de l'achat : " << date_achat << std::endl;
    std::cout << "Produits achetés :" << std::endl;
    for (int i = 0; i < nombreProduits; ++i) {
        produits[i]->afficher();
    }
    std::cout << "Personnes concernées :" << std::endl;
    for (int i = 0; i < nombrePersonnes; ++i) {
        personnes[i]->afficher();
    }
}
