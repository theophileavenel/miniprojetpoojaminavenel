#ifndef ACHAT_H
#define ACHAT_H

#include "Produit.h"
#include "Personne.h"

class Achat {
private:
    int date_achat;
    Produit* produits[MAX_PRODUITS];
    int nombreProduits;

    Personne* personnes[MAX_PERSONNES];
    int nombrePersonnes;

public:
    Achat(int date);
    ~Achat();
    void ajouterProduitAchat(Produit* produit);
    void ajouterPersonneAchat(Personne* personne);
    void afficher() const;
};

#endif
