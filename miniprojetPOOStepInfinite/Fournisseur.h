#ifndef FOURNISSEUR_H
#define FOURNISSEUR_H

#include <iostream>
#include <string>
#include "Produit.h"
#include "constants.h"

class Fournisseur {
private:
    std::string nom;
    Produit* produits[MAX_PRODUITS];
    int nbProduits;

public:
    Fournisseur(const std::string& nom);
    void ajouterProduit(Produit* produit);
    void afficher() const;
};

#endif // FOURNISSEUR_H
