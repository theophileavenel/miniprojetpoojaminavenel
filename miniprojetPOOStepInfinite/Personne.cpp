#include "Personne.h"
#include "Achat.h"

// Implémentation de la classe Personne
Personne::Personne(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
    : datenaissance(datenaissance), numerosecuritesociale(numerosecuritesociale), adressemail(adressemail), nom(nom), prenom(prenom) {}

Personne::~Personne() {
    //std::cout << "Le destructeur va supprimer l'objet de type Personne." << std::endl;
}

// Implémentation de la classe Personnel
Personnel::Personnel(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
                     double salaire, int datedebut, int datefin, const std::string& role)
    : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), salaire(salaire), datedebut(datedebut), datefin(datefin), role(role) {}

Personnel::~Personnel() {
    std::cout << "Le destructeur va supprimer l'objet de type Personnel." << std::endl;
}

void Personnel::afficher() const {
    std::cout << "Bonjour, je suis " << prenom << " " << nom
              << " et je suis né en " << datenaissance // Sa date de naissance (on va utiliser l’année seulement pour ce projet)
              << ". Mon adresse mail est : " << adressemail 
              << ". Je suis membre du personnel depuis l'année " << datedebut 
              << " et ma date de fin de travail est " << datefin 
              << ". Mon rôle est " << role 
              << " et mon salaire est de " << salaire
              << std::endl;
}


// Constructeur de la classe Client
Client::Client(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom)
    : Personne(datenaissance, numerosecuritesociale, adressemail, nom, prenom), nombreAchats(0) {}

// Destructeur de la classe Client pour gérer la libération de mémoire
Client::~Client() {
    std::cout << "Le destructeur va supprimer l'objet de type Client." << std::endl;
    for (int i = 0; i < nombreAchats; ++i) {
        delete anciensAchats[i];
    }
}

// Méthode pour ajouter un achat dans le tableau des anciens achats
void Client::ajouterAchat(Achat* achat) {
    if (nombreAchats < MAX_ACHATS) {
        anciensAchats[nombreAchats++] = achat;
    } else {
        std::cout << "Liste d'achats pleine, impossible d'ajouter plus d'achats." << std::endl;
    }
}

// Méthode pour afficher les achats du client
void Client::afficherAchats() const {
    std::cout << "Achats de " << nom << " :" << std::endl;
    for (int i = 0; i < nombreAchats; ++i) {
        anciensAchats[i]->afficher();
    }
}

// Méthode pour afficher les informations du client
void Client::afficher() const {
    std::cout << "Bonjour, je suis " << prenom << " " << nom
              << " et je suis né en " << datenaissance
              << ". Mon adresse mail est : " << adressemail 
              << ". Mes anciens achats sont :" << std::endl;
    afficherAchats();
} 