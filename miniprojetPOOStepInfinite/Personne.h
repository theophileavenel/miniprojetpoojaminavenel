#ifndef PERSONNE_H
#define PERSONNE_H

#include <iostream>
#include <string>
#include "constants.h"

// Déclaration avancée de la classe Achat
class Achat;

// Classe abstraite Personne
class Personne {
protected:
    int datenaissance; // Sa date de naissance (on va utiliser l’année seulement pour ce projet)
    int numerosecuritesociale;
    std::string adressemail;
    std::string nom;
    std::string prenom;
public:
    Personne(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom);

    virtual void afficher() const = 0; // Méthode pure virtuelle pour définir la classe abstraite

    virtual ~Personne();
};

// Classe Personnel
class Personnel : public Personne {
private:
    double salaire;
    int datedebut;
    int datefin;
    std::string role;

public:
    Personnel(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom,
              double salaire, int datedebut, int datefin, const std::string& role);

    ~Personnel();

    void afficher() const ;
};

// Classe Client
class Client : public Personne {
private:
    Achat* anciensAchats[MAX_ACHATS];
    int nombreAchats;

public:
    Client(int datenaissance, int numerosecuritesociale, std::string adressemail, std::string nom, std::string prenom);
    ~Client(); // Pour gérer la libération de mémoire
    void ajouterAchat(Achat* achat);
    void afficherAchats() const;
    void afficher() const;
};

#endif // PERSONNE_H
