#include "Produit.h"

// Implémentation de la classe Produit
Produit::Produit(int numeroserie, int quantite, double prixunitaire)
    : numeroserie(numeroserie), quantite(quantite), prixunitaire(prixunitaire) {}

Produit::~Produit() {
    //std::cout << "Le destructeur va supprimer l'objet de type Produit." << std::endl;
}

// Implémentation de la classe ProduitAlimentaire
ProduitAlimentaire::ProduitAlimentaire(int numeroserie, int quantite, double prixunitaire, const int datePeremption)
    : Produit(numeroserie, quantite, prixunitaire), datePeremption(datePeremption) {}

ProduitAlimentaire::~ProduitAlimentaire() {
    std::cout << "Le destructeur va supprimer l'objet de type ProduitAlimentaire." << std::endl;
}

void ProduitAlimentaire::afficher() const {
    std::cout << "Produit Alimentaire - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Date de péremption: " << datePeremption << std::endl;
}

// Implémentation de la classe ProduitElectrique
ProduitElectrique::ProduitElectrique(int numeroserie, int quantite, double prixunitaire, double puissance, int dureeGarantie)
    : Produit(numeroserie, quantite, prixunitaire), puissance(puissance), dureeGarantie(dureeGarantie) {}

ProduitElectrique::~ProduitElectrique() {
    std::cout << "Le destructeur va supprimer l'objet de type ProduitElectrique." << std::endl;
}

void ProduitElectrique::afficher() const {
    std::cout << "Produit Electrique - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Puissance: " << puissance 
              << "W, Durée de garantie: " << dureeGarantie << " ans" << std::endl;
}

// Implémentation de la classe Jeu
Jeu::Jeu(int numeroserie, int quantite, double prixunitaire, int ageMinimum)
    : Produit(numeroserie, quantite, prixunitaire), ageMinimum(ageMinimum) {}

Jeu::~Jeu() {
    std::cout << "Le destructeur va supprimer l'objet de type Jeu." << std::endl;
}

void Jeu::afficher() const {
    std::cout << "Jeu - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Age minimum: " << ageMinimum << " ans" << std::endl;
}

// Implémentation de la classe Logiciel
Logiciel::Logiciel(int numeroserie, int quantite, double prixunitaire, const std::string& systemeExploitation, const std::string& caracteristiquesNecessaires, const std::string& categorie)
    : Produit(numeroserie, quantite, prixunitaire), systemeExploitation(systemeExploitation), caracteristiquesNecessaires(caracteristiquesNecessaires), categorie(categorie) {}

Logiciel::~Logiciel() {
    std::cout << "Le destructeur va supprimer l'objet de type Logiciel." << std::endl;
}

void Logiciel::afficher() const {
    std::cout << "Logiciel - Numéro de série: " << numeroserie 
              << ", Quantité: " << quantite 
              << ", Prix unitaire: " << prixunitaire 
              << ", Système d'exploitation: " << systemeExploitation 
              << ", Caractéristiques nécessaires: " << caracteristiquesNecessaires 
              << ", Catégorie: " << categorie << std::endl;
}

