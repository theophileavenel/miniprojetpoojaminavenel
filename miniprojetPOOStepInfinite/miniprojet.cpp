#include <iostream>
#include "Fournisseur.h"
#include "Personne.h"
#include "Produit.h"
#include "Achat.h"

//g++ -g miniprojet.cpp Fournisseur.cpp Produit.cpp Personne.cpp Achat.cpp -o miniprojet

//gdb ./miniprojet 

//run

//exit

int main() {
    // Création des objets Produit
    Produit* p1 = new ProduitAlimentaire(101, 50, 3.99, 2024);
    Produit* p2 = new ProduitElectrique(102, 20, 99.99, 1500, 2);
    Produit* p3 = new Jeu(103, 30, 49.99, 12);
    Produit* p4 = new Logiciel(104, 10, 199.99, "Windows 10", "8GB RAM, 500GB Disk", "Bureautique");

    // Affichage des produits
    p1->afficher();
    p2->afficher();
    p3->afficher();
    p4->afficher();

    // Création des objets Personnel
    Personnel* pers1 = new Personnel(1975, 123456789, "john.doe@example.com", "Doe", "John", 50000, 20100101, 20250101, "Manager");

    // Création des objets clients
    Client* cli1 = new Client(1990, 987654321, "jane.doe@example.com", "Doe", "Jane");

    // Création des objets Fournisseur
    Fournisseur* four1 = new Fournisseur("Epic Games");
    Fournisseur* four2 = new Fournisseur("Amazon");
    Fournisseur* four3 = new Fournisseur("Intermarche");

    // Ajout des produits au fournisseur
    four1->ajouterProduit(p3);
    four2->ajouterProduit(p2);
    four3->ajouterProduit(p1);
    four3->ajouterProduit(p4);

    // Affichage des fournisseurs et leurs produits
    four1->afficher();
    four2->afficher();
    four3->afficher();

    // Création des achats
    Achat* achat1 = new Achat(20230522); // Date au format AAAAMMJJ
    achat1->ajouterProduitAchat(p1);
    achat1->ajouterPersonneAchat(cli1);

    Achat* achat2 = new Achat(20230523);
    achat2->ajouterProduitAchat(p2);
    achat2->ajouterProduitAchat(p3);
    achat2->ajouterPersonneAchat(cli1);
    achat2->ajouterPersonneAchat(pers1);

    // Ajout des achats aux clients
    cli1->ajouterAchat(achat1);
    cli1->ajouterAchat(achat2);

    // Affichage des achats du client
    cli1->afficher();

    // Suppression des objets pour libérer la mémoire
    delete p1;
    delete p2;
    delete p3;
    delete p4;
    delete pers1;
    delete cli1;
    delete four1;
    delete four2;
    delete four3;
    delete achat1;
    delete achat2;

    return 0;
}
