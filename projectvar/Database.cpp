#include "Database.h"
#include <iostream>

// Constructeur : ouvre la base de données SQLite
Database::Database(const std::string& dbName) {
    // Ouvre la base de données avec le nom donné
    if (sqlite3_open(dbName.c_str(), &db) != SQLITE_OK) {
        // Si l'ouverture échoue, lance une exception avec le message d'erreur
        throw std::runtime_error("Can't open database: " + std::string(sqlite3_errmsg(db)));
    }
}

// Destructeur : ferme la base de données SQLite
Database::~Database() {
    // Ferme la connexion à la base de données
    sqlite3_close(db);
}

// Méthode pour exécuter une requête SQL sans récupérer de résultats
void Database::executeQuery(const std::string& query) {
    char* zErrMsg = nullptr;
    // Exécute la requête SQL
    int rc = sqlite3_exec(db, query.c_str(), nullptr, 0, &zErrMsg);
    if (rc != SQLITE_OK) {
        // Si une erreur se produit, stocke le message d'erreur et libère la mémoire
        std::string errorMsg = zErrMsg;
        sqlite3_free(zErrMsg);
        // Lance une exception avec le message d'erreur
        throw std::runtime_error("SQL error: " + errorMsg);
    }
}

// Méthode pour récupérer un résultat de type chaîne de caractères à partir d'une requête SELECT
std::string Database::getStringResult(const std::string& query) {
    sqlite3_stmt* stmt;
    std::string result;

    // Prépare la requête SQL pour exécution
    // La requête préparée permet de compiler la requête SQL une fois et de l'exécuter plusieurs fois
    if (sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr) != SQLITE_OK) {
        // Si la préparation échoue, lance une exception avec le message d'erreur
        throw std::runtime_error("Failed to prepare statement: " + std::string(sqlite3_errmsg(db)));
    }

    // Exécute la requête et vérifie si un résultat est disponible
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        // Récupère le résultat de la première colonne (indice 0)
        result = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0));
    }

    // Finalise la requête et libère les ressources associées
    // Cela libère la mémoire allouée pour le sqlite3_stmt
    sqlite3_finalize(stmt);
    return result;
}

// Méthode pour récupérer plusieurs résultats sous forme de tableau à partir d'une requête SELECT
std::vector<std::vector<std::string>> Database::getTableResult(const std::string& query) {
    sqlite3_stmt* stmt;
    std::vector<std::vector<std::string>> results;

    // Prépare la requête SQL pour exécution
    // La requête préparée permet d'éviter l'injection SQL et améliore la performance
    if (sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr) != SQLITE_OK) {
        // Si la préparation échoue, lance une exception avec le message d'erreur
        throw std::runtime_error("Failed to prepare statement: " + std::string(sqlite3_errmsg(db)));
    }

    // Récupère le nombre de colonnes dans le résultat
    int cols = sqlite3_column_count(stmt);

    // Exécute la requête et boucle sur chaque ligne de résultat
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        std::vector<std::string> row;
        // Boucle sur chaque colonne de la ligne
        for (int col = 0; col < cols; ++col) {
            // Récupère le texte de la colonne courante
            const char* text = reinterpret_cast<const char*>(sqlite3_column_text(stmt, col));
            // Ajoute le texte à la ligne, en traitant les valeurs NULL comme des chaînes vides
            row.push_back(text ? text : "");
        }
        // Ajoute la ligne au tableau des résultats
        results.push_back(row);
    }

    // Finalise la requête et libère les ressources associées
    // Cela libère la mémoire allouée pour le sqlite3_stmt
    sqlite3_finalize(stmt);
    return results;
}
