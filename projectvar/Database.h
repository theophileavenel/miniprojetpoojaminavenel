#ifndef DATABASE_H
#define DATABASE_H

#include <sqlite3.h>
#include <string>
#include <vector>
#include <stdexcept>

// Classe pour gérer les opérations de la base de données SQLite
class Database {
    private:
        // Pointeur vers la base de données SQLite
        sqlite3* db;
    public:
        // Constructeur qui ouvre la base de données
        Database(const std::string& dbName);
        
        // Destructeur qui ferme la base de données
        ~Database();
        
        // Méthode pour exécuter une requête SQL sans récupérer de résultats
        void executeQuery(const std::string& query);
        
        // Méthode pour récupérer un résultat de type chaîne de caractères à partir d'une requête SELECT
        std::string getStringResult(const std::string& query);
        
        // Méthode pour récupérer plusieurs résultats sous forme de tableau à partir d'une requête SELECT
        std::vector<std::vector<std::string>> getTableResult(const std::string& query);
};

#endif // DATABASE_H
