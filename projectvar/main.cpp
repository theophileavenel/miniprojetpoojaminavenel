#include <iostream>
#include "Database.h"

//g++ -o myProgram main.cpp Database.cpp -lsqlite3
// ./myProgram


#include <iostream>
#include "Database.h"

int main() {
    try {
        // Création d'une instance de la base de données
        Database db("data.db");

        // Création de la table "inscrit" si elle n'existe pas déjà
        db.executeQuery("CREATE TABLE IF NOT EXISTS inscrit (numid INTEGER PRIMARY KEY, nom TEXT, age INTEGER);");

        // Insertion des données dans la table "inscrit"
        db.executeQuery("INSERT INTO inscrit (nom, age) VALUES ('Alice', 30);");
        db.executeQuery("INSERT INTO inscrit (nom, age) VALUES ('Bob', 25);");

        // Obtention d'un résultat spécifique (nom de l'inscrit avec numid = 1)
        std::string name = db.getStringResult("SELECT nom FROM inscrit WHERE numid = 1;");
        std::cout << "Name with numid 1: " << name << std::endl;

        // Obtention des résultats sous forme de table (tous les inscrits)
        std::vector<std::vector<std::string>> results = db.getTableResult("SELECT * FROM inscrit;");
        
        // Affichage des résultats
        for (const auto& row : results) {
            for (const auto& col : row) {
                std::cout << col << " "; // Affiche chaque colonne de la ligne
            }
            std::cout << std::endl; // Nouvelle ligne pour chaque enregistrement
        }
    } catch (const std::exception& e) {
        // Gestion des exceptions : affiche le message d'erreur
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
